<?php
// $db_gallery : access db object
// $globals : globals settings
// $actual_srv = params defined by system

if (isset($actual_srv["enable"]) && strlen($actual_srv["enable"])) {
    $arrArticle = unserialize (ARTICLE_SELECTED_TEST);
    if(
        strpos($_SERVER["PATH_INFO"], "/coronavirus") === 0
    ) {
        $force_open = false;
        $cm->oPage->tplAddJs("coronavirus-script", "coronavirus-chat.js", FF_THEME_DIR ."/" . FRONTEND_THEME . "/javascript", true, $oPage->isXHR(), null, false, "bottom");
        $cm->oPage->tplAddCss("corona-css", "corona-chatbot.css", "/themes/site/css");

    }
} 