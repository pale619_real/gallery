<?php
/**
 *   VGallery: CMS based on FormsFramework
Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * @package VGallery
 * @subpackage module
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link https://github.com/wolfgan43/vgallery
 */
define('CMS_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Load Config base
|--------------------------------------------------------------------------
|
|
|
*/
$_SERVER["HTTPS"] = "on";

require_once("config.php");

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

if (defined("COMPOSER_PATH") && COMPOSER_PATH) {
    require_once(__DIR__ . COMPOSER_PATH . "/autoload.php");
}


/*
|--------------------------------------------------------------------------
| Register CMS Auto Loader
|--------------------------------------------------------------------------
|
|
|
*/
//require_once (__CMS_DIR__ . "/library/gallery/models/autoload.php");
require_once(__DIR__ . "/hook.php");


/*
|--------------------------------------------------------------------------
| Init
|--------------------------------------------------------------------------
|
| Rewrite PathInfo
| Resolve Request
| Load Settings
|
| Run router
|
*/
$kernel = Cms::getInstance("kernel");

$kernel->run();
exit;



require_once(__DIR__ . "/library/gallery/system/cache.php");

/**
 * Cache System
 */
$cache = check_static_cache_page();



//if(DEBUG_MODE === true)
    set_time_limit(20);

$path_info = $_SERVER["PATH_INFO"];
if ($path_info == "/index") {
    $path_info = "";
}

/**
 * Check Redirect
 */
if ($_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest") {
    cache_check_redirect($path_info);
}

/**
 * Log File Without Cache
 */
Cache::log($path_info, "access");
$marco = "";

/**
 * Run page Without Cache
 */

require_once("cm/main.php");
