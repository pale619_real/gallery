if(!hCore) var hCore = {
    auth : {}
};

jQuery.extend(true, hCore.auth, {
    bExpire : 300000,
    bearer : "",
    expire: "",
    activation : function (url, selector, elem, ret_url, resendCode, type) {
        var selectorID = (selector
                ? "#" + selector
                : document
        );

        var domain = jQuery(selectorID).find("INPUT[name='domain']").val() || undefined;
        var email = jQuery(selectorID).find("INPUT[name='user-email']").val() || undefined;
        var token = jQuery(selectorID).find("INPUT[name='csrf']").val() || "";
        var verifyCode = jQuery(selectorID).find("INPUT[name='codice-conferma']").val() || "";
        var reloadUrl = "window.location.href = '" + url + "';"
        var reloadLink = ' <a class="resend-code" href="javascript:void(0);" onclick="' + reloadUrl + '" style="color:#009fe3;cursor:pointer;">Email errata? Ricomincia</a>';
        var resendUrl = "hCore.auth.activation('" + url + "', '" + selector + "', '" + elem + "', '" + ret_url + "', true);";
        var resendLink = ' <a class="resend-code" href="javascript:void(0);" onclick="' + resendUrl + '" style="color:#009fe3;cursor:pointer;">Manda un nuovo codice</a>';
        if(resendCode) {
            jQuery(selectorID).find("INPUT[name='codice-conferma']").val('');
            hCore.auth.setBearer();
        }
        var bearer = jQuery(selectorID).find("INPUT[name='bearer']").val() || "";
        if(bearer) {
            this.setBearer(bearer);
        }

        var data = {};
        var headers = {};

        if(!url)
            url = '/activation';

        if(ret_url)
            url = ff.urlAddParam(url, "redirect", ret_url);

        if(this.expire <= new Date().getTime()) {
            this.bearer = "";
            this.expire = "";
        }

        if(verifyCode.length) {
            headers = {
                "Bearer" : this.bearer
                , "domain": domain
                , "csrf": token
            };
            data = {
                 "t": $.trim(verifyCode)
                ,"username": email
                //, "key" : value
                //, "scopes" : "activation"
            };
        } else {
            headers = {
                "domain": domain
                , "csrf": token
            };
            data = {
                "username": email
                , "type": type 
                //, "scopes" : "activation"
            };
        }

        $(selectorID + " .error-container").html('');
        jQuery(elem).css({
            'opacity': '0.5'
        }).attr("disabled", "disabled");

        $.ajax({
            url: url,
            headers: headers,
            method: 'POST',
            dataType: 'json',

            data: data,
            success: function(data) {
                jQuery(elem).css({
                    'opacity': ''
                }).removeClass("disabled");

                if(data.status === "0") { 
                    if(data["redirect"] !== undefined) {
                        $(selectorID + " .error-container").html('<div class="callout success">' + 'Il tuo account è stato attivato!' + '</div>');

                        if(data["redirect"].length) {
                            window.location.href = data["redirect"];
                        } else {
                            window.location.href = "/user";
                        }
                    } else {
                        hCore.auth.setBearer(data["t"]);
                        if(!data["sender"]) {
                            data["sender"] = "email";
                        }

                        if(data["sender"]) {
                            $(selectorID + " .error-container").html('<div class="callout success">' + 'Controlla la tua ' + data["sender"] + reloadLink + '</div>');
                            jQuery(selectorID).find(".hide-code-string").removeClass("hide-code-string");
                            jQuery(selectorID).find("INPUT[name='user-email']").prop('disabled', true);
                        }
                    }
                } else {
                    if($(selectorID + " .error-container").length) {
                        $(selectorID + " .error-container").html('<div class="callout alert">' + data.error + (data.status === "404" ? " " + resendLink : "") + '</div>');
                    }
                }
                $(elem).find(".disabled").css({'opacity': '', 'pointer-events': ''}).removeClass("disabled");
            }
        });

        return false;
    },
    "submitProcessKey" : function (elem) {
        var e = window.event;
        if (e.keyCode == 13)  {
            $(elem).next().focus();
            $(elem).next().click();
            return false;
        }
    },
    "getURLParameter" : function(name) {
        return decodeURIComponent(
            (RegExp(name.replace(/\[/g, "\\[").replace(/\]/g, "\\]") + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
        );
    },
    "setBearer" : function(t) {
        if(t) {
            this.bearer = t;
            this.expire = new Date().getTime() + this.bExpire;
        } else {
            this.bearer = "";
            this.expire = "";
        }
    },
    "setVerifyCode" : function(code) {
        if(code) {
            this.verifyCode = code;
        } else {
            this.verifyCode = "";
        }
    }

});