<?php
/**
*   VGallery: CMS based on FormsFramework
    Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * @package VGallery
 * @subpackage core
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
* @license http://opensource.org/licenses/lgpl-3.0.html
* @link https://bitbucket.org/cmsff/vgallery
 */

class Stats extends vgCommon
{
	static $singleton                   = null;
	private $service					= null;
    protected $controllers              = null;
    protected $controllers_rev          = null;
    protected $connectors               = array();
    protected $struct                   = array();
    private $driver						= null;
    private $result						= null;

    /**
     * @param $service
     * @return null|Stats
     */
    public static function getInstance($service)
	{
		if (self::$singleton === null)
			self::$singleton = new Stats($service);
		else {
			self::$singleton->service = $service;
		}
		return self::$singleton;
	}

    /**
     * @param null $start
     * @return mixed|string
     */
    public static function stopwatch($start = null) {
		if(!$start)
			return microtime(true);

		$duration = microtime(true) - $start;
		return number_format($duration, 2, '.', '');
	}

    public static function getVisitor($key = null, $user_agent = null) {
        $visitor                        = false;
        if($user_agent === null) {
            $user_agent = $_SERVER["HTTP_USER_AGENT"];
        }

        if(!self::isCrawler($user_agent)) {
            $long_time = time() + (60 * 60 * 24 * 365 * 30);

            if($_COOKIE["_ga"]) {
                $ga = explode(".", $_COOKIE["_ga"]);

                $visitor = array(
                    "unique"        => $ga[2]
                    , "created"     => $ga[3]
                    , "last_update" => $ga[3]
                );
            } elseif($_COOKIE["__utma"]) {
                $utma = explode(".", $_COOKIE["__utma"]);

                $visitor = array(
                    "unique" => $utma[1]
                , "created" => $utma[2]
                , "last_update" => $utma[4]
                );
            } elseif($_COOKIE["_uv"]) {
                $uv = explode(".", $_COOKIE["_uv"]);

                $visitor = array(
                    "unique" => $uv[0]
                , "created" => $uv[1]
                , "last_update" => $uv[2]
                );
                if($visitor["last_update"] + (60 * 60 * 24) < time()) {
                    $visitor["last_update"] = time();

                    //$_COOKIE["_uv"] = implode(".", $visitor);
                    setcookie("_uv", implode(".", $visitor), $long_time);
                }
            } else {
                $access = explode("E", hexdec(md5(
                    $_SERVER["REMOTE_ADDR"]
                    . $_SERVER["HTTP_USER_AGENT"]
                )));

                $offset = (strlen($access[0]) - 9);
                $visitor = array(
                    "unique" => substr($access[0], $offset, 9)
                    , "created" => time()
                    , "last_update" => time()
                );
                //$_COOKIE["_uv"] = implode(".", $visitor);
                setcookie("_uv", implode(".", $visitor), $long_time);
            }


        }

        return ($key
            ? $visitor[$key]
            : $visitor
        );
    }

    public static function isCrawler($user_agent = null)
    {
        $isCrawler = true;
        $crawlers = array(
            'Google'=>'Google',
            'MSN' => 'msnbot',
            'Rambler'=>'Rambler',
            'Yahoo'=> 'Yahoo',
            'AbachoBOT'=> 'AbachoBOT',
            'accoona'=> 'Accoona',
            'AcoiRobot'=> 'AcoiRobot',
            'ASPSeek'=> 'ASPSeek',
            'CrocCrawler'=> 'CrocCrawler',
            'Dumbot'=> 'Dumbot',
            'FAST-WebCrawler'=> 'FAST-WebCrawler',
            'GeonaBot'=> 'GeonaBot',
            'Gigabot'=> 'Gigabot',
            'Lycos spider'=> 'Lycos',
            'MSRBOT'=> 'MSRBOT',
            'Altavista robot'=> 'Scooter',
            'AltaVista robot'=> 'Altavista',
            'ID-Search Bot'=> 'IDBot',
            'eStyle Bot'=> 'eStyle',
            'Scrubby robot'=> 'Scrubby',
            'Screaming SEO bot'=> 'Screaming Frog',
            'GenericBot' => 'bot',
            'GenericSEO' => 'seo',
            'GenericCrawler' => 'crawler'
        );

        if($user_agent === null)
            $user_agent = $_SERVER["HTTP_USER_AGENT"];

        if($user_agent) {
            $crawlers_agents = implode("|", $crawlers);
            $isCrawler = (preg_match("/" . $crawlers_agents . "/i", $user_agent) > 0);
        }


        return $isCrawler;
    }

    /**
     * Stats constructor.
     * @param $service
     */
    public function __construct($service)
	{
	    $this->loadControllers(__DIR__);

		$this->service = $service;
	}

    /**
     * @param $where
     * @param null $fields
     * @return null
     */
    public function read($where, $fields = null) {
		$this->clearResult();

		$this->result[$this->service] = $this->getDriver()->get_stats($where, null, $fields);
		$this->result = $this->result[$this->service];

		return $this->getResult();
	}

    /**
     * @param $where
     * @param null $set
     * @return null
     */
    public function update($where, $set = null) {
		$this->clearResult();

		$this->result[$this->service] = $this->getDriver()->get_stats($where, $set);
		$this->result = $this->result[$this->service];

		return $this->getResult();
	}

    /**
     * @param null $insert
     * @param null $update
     * @return null
     */
    public function write($insert = null, $update = null) {
		$this->clearResult();

		$this->result[$this->service] = $this->getDriver()->write_stats($insert, $update);
		$this->result = $this->result[$this->service];

		return $this->getResult();
	}

    /**
     * @param null $insert
     * @param null $update
     * @return null
     */
    /*public function writePartial($url) {
		$this->clearResult();

		$this->result[$this->service] = $this->getDriver()->write_stats_partial($url);
		$this->result = $this->result[$this->service];

		return $this->getResult();
	}*/

    /**
     * @param $where
     * @return null
     */
    public function delete($where) {
		$this->clearResult();

		return $this->getResult();
	}

    /**
     * @param null $fields
     * @param null $where
     * @return null
     */
    public function get($fields = null, $where = null, $table = null) {
		$this->clearResult();
		if(!$where && $fields && !$table) {
			$where = $fields;
			$fields = null;
		} elseif($fields && $where && !is_array($fields) && !is_array($where) && !$table) {
            $table = $where;
            $where = $fields;
            $fields = null;

        }
        if(!$table) $table = "user_vars";

		$this->result = $this->getDriver()->get_vars($where, $fields, $table);
		return $this->getResult();
	}

    /**
     * @param $rules
     * @param $where
     * @return null
     */
    public function like($rules, $where) {
		$this->clearResult();

		$match = array();
		if($rules) {
			if (!is_array($rules))
				$rules = array($rules);

			$data = $this->getDriver()->get_vars($where);
			if (is_array($data) && count($data)) {
				foreach ($rules AS $rule) {
					$match = $match + (array)preg_grep("/^" . str_replace(array("\*", "\?"), array("(.+)", "(.?)"), preg_quote($rule)) . "$/i", array_keys($data));
				}
			}

			if (count($match))
				$this->result = array_intersect_key($data, array_fill_keys($match, true));
		}

		return $this->getResult();
	}

    /**
     * @param $rules
     * @param $where
     * @return null
     */
    public function sum($rules, $where, $table = "user_vars") {
		$this->clearResult();

		$this->result = $this->getDriver()->sum_vars($where, $rules, $table);

		return $this->getResult();
	}

    /**
     * @param $when
     * @param $fields
     * @param null $where
     * @return null
     * @throws Exception
     */
    public function range($when, $fields, $where = null) {
		$this->clearResult();
        $range                                                              = null;
        $model                                                              = null;
        $pos                                                                = null;
        $operation                                                          = null;
		if(!$where) {
			$this->isError("class not support method. todo: we need to create average stats. Please try later. ^_^");
		} else {
			if ($when) {
			    if($fields) {
                    if (!is_array($fields))
                        $fields                                             = array($fields);
                } else {
			        $this->isError("fields empty");
                }

				if (is_array($when)) {
                    $operation                                              = "get";
					$range                                                  = $when;
                    if (is_array($fields) && count($fields)) {
                        foreach ($range as $key => $value) {
                            foreach ($fields AS $field) {
                                $rules[]                                    = $field . "-" . $value->format('Y-m-d');

                                $res[$field]                                = array();
                            }
                        }
                    }
				} elseif(strpos($when, " ") !== false) {
                    $operation                                              = "get";
                    $arrPeriod                                              = explode(" ", $when);
                    $arrPeriod["start"]                                     = new DateTime($arrPeriod[0]);
                    $arrPeriod["end"]                                       = new DateTime($arrPeriod[1]);
                    $arrPeriod["end"]                                       = $arrPeriod["end"]->modify("+1 day");
                    $arrPeriod["interval"]                                  = new DateInterval('P1D');
                    $range                                                  = new DatePeriod(
                                                                                $arrPeriod["start"]
                                                                                , $arrPeriod["interval"]
                                                                                , $arrPeriod["end"]
                                                                            );
                    if (is_array($fields) && count($fields)) {
                        foreach ($range as $key => $value) {
                            foreach ($fields AS $field) {
                                $rules[]                                    = $field . "-" . $value->format('Y-m-d');

                                $res[$field]                                = 0;
                            }
                        }
                    }
                } else {
                    $operation                                              = "sum";
                    $time                                                   = strtotime($when . str_repeat("-01", 2 - substr_count($when, "-")));
                    if ($time) {
                        $arrTime                                            = getdate($time);

                        $range                                              = array(
                                                                                "year"          => $arrTime["year"]
                                                                                , "month"       => (substr_count($when, "-") >= 1
                                                                                                    ? str_pad($arrTime["mon"], 2, "0", STR_PAD_LEFT)
                                                                                                    : "0"
                                                                                                )
                                                                                , "day"         => (substr_count($when, "-") == 2
                                                                                                    ? str_pad($arrTime["mday"], 2, "0", STR_PAD_LEFT)
                                                                                                    : "0"
                                                                                                )
                                                                            );
                    }

                    if (is_numeric($range["year"]) && is_numeric($range["month"]) && $range["day"]) {
                        $model                                              = "0";
                    } elseif ($range["year"] && $range["month"]) {
                        $model                                              = array_fill(0, cal_days_in_month(CAL_GREGORIAN, $range["month"], $range["year"]), "0");
                        $pos                                                = 3;
                    } elseif ($range["year"]) {
                        $model                                              = array_fill(0, 12, "0");
                        $pos                                                = 2;
                    } else {
                        $this->isError("invalid range time");
                    }

                    if (!$this->isError()) {
                        $tRule                                              = $range["year"];
                        if ($range["month"]) {
                            $tRule                                          .= "-" . str_pad($range["month"], 2, "0", STR_PAD_LEFT);
                            if ($range["day"]) {
                                $tRule                                      .= "-" . str_pad($range["day"], 2, "0", STR_PAD_LEFT);
                            } else {
                                $tRule                                      .= "-*";
                            }
                        } else {
                            $tRule                                          .= "-??";
                        }

                        if (is_array($fields) && count($fields)) {
                            foreach ($fields AS $field) {
                                $rules[]                                    = $field . "-" . $tRule;

                                $res[$field]                                = $model;
                            }
                        }

                    }
                }

                if($rules) {
                    $data                                                   = $this->$operation($rules, $where);

                    if (is_array($data) && count($data)) {
                        if($this->isAssocArray($data)) {
                            $res                                            = $this->getRangeValue($data, $pos, $res);
                        } else {
                            foreach ($data AS $key => $value) {
                                $res                                        = $this->getRangeValue($value, $pos, $res);
                            }
                        }
                    }

                    $this->result                                           = (count($fields) > 1
                                                                                ? $res
                                                                                : $res[$fields[0]]
                                                                            );
                }
			} else {
				$this->isError("when empty");
			}
		}

		return $this->getResult();
	}

    /**
     * @param $set
     * @param null $where
     * @param null $old
     * @return null
     */
    public function set($set, $where = null, $table = null) {
		$this->clearResult();

        if(!$table) $table = "user_vars";
        
		$this->result = $this->getDriver()->set_vars($set, $where, $table);
		return $this->getResult();
	}

    /**
     * @param $type
     * @param null $config
     * @return array|mixed|null
     */
    public function getConfig($type, $config = null) {
		if(!$config)
			$config = $this->services[$type]["connector"];

		if(is_array($config))
			$config = array_replace($this->connectors[$type], array_filter($config));
		else
			$config = $this->connectors[$type];

		return $config;
	}

    /**
     * @param $set
     * @param null $old
     * @return null
     */
    public function normalize_fields($set, $old = null) {
        $def = $this->getDriver()->getStruct();

        if(is_array($set) && count($set)) {
            if($old)
                $res = $old;

            foreach ($set AS $key => $value) {
                if(is_array($value)) {
                    if(!count($value)) {
                        $res[$key] = ($old[$key]
                                        ? $old[$key]
                                        : array()
                                    );
                    } elseif($def["struct"][$key] === "arrayIncremental") {
                        $last = $res[$key][count($res[$key]) - 1];

                        if($value != $last)
                            $res[$key][] = $value;

                    } else {
                        $res[$key] = $this->normalize_fields($value, $old[$key]);
                    }
                } else {

if($value === "++") {
    $res[$key] = ($old[$key]
        ? ++$old[$key]
        : 1
    );
}else if($value === "--") {
    $res[$key] = ($old[$key]
        ? --$old[$key]
        : 0
    );
}else{
    $res[$key] = $value;
}
                   /* switch ($value) {
                        //case "+":
                        //    $res[$key][] = $value;
                        //    break;
                        case "++":{
                            echo ' _ENTRA ++';

                            break;}
                        case "--":{
                            echo ' _ENTRA --';

                            break;}
                        default:{
                            echo ' _ENTRA deffault';
                            }
                    }*/
                }
            }
        }
        return $res;
    }

    /**
     * @return mixed
     */
    private function getDriver() {
		return $this->controller();
	}
	/**
	 * @param null $service
	 */
	private function controller()
	{
		$type                                                           	= $this->service;

		if(!$this->driver[$type]) {
			$controller                                                 	= "stats" . ucfirst($type);
			//require_once($this->getAbsPathPHP("/stats/services/" . $type, true));

			$driver                                                     	= new $controller($this);
			//$db                                                         	= $driver->getDevice();

			$this->driver[$type] 											= $driver;
		}

		return $this->driver[$type];
	}

    /**
     *
     */
    private function clearResult()
	{
		$this->result = array();
		$this->isError("");
	}

    /**
     * @return null
     */
    private function getResult()
	{
		return ($this->isError()
			? $this->isError()
			: $this->result
		);
	}

    /**
     * @param $data
     * @param null $pos
     * @param array $res
     * @return array
     */
    private function getRangeValue($data, $pos = null, $res = array()) {
        foreach ($data AS $key => $value) {
            $arrKey                                                         = explode("-", $key);
            if ($pos)
                $res[$arrKey[0]][(int)$arrKey[$pos] - 1]                    = $value;
            else
                $res[$arrKey[0]]                                            += $value;
        }

        return $res;
    }
}

