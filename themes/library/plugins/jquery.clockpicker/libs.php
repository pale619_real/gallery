<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"clockpicker" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.clockpicker",
							"file" => "jquery.clockpicker.min.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.clockpicker",
									"file" => "jquery.clockpicker.min.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
