<?php
// $db_gallery : access db object
// $globals : globals settings
// $actual_srv = params defined by system

if (isset($actual_srv["enable"]) && strlen($actual_srv["enable"])) {
    switch ($globals->user_path) {
        case "/OneSignalSDKUpdaterWorker.js":
        case "/OneSignalSDKWorker.js":
            header("Content-type: application/javascript");

            echo "importScripts('https://cdn.onesignal.com/sdks/OneSignalSDK.js');";
            exit;
        case "/new-login-test":
        case "/libretto-sanitario-coronavirus":
        case "/libretto-sanitario":
            break;
        default:
            $globals->manifest["name"] = ($actual_srv["name"] ? $actual_srv["name"] : CM_LOCAL_APP_NAME);
            $globals->manifest["short_name"] = ($actual_srv["short_name"] ? $actual_srv["short_name"] : CM_LOCAL_APP_NAME);
            $globals->manifest["start_url"] = "/";
            $globals->manifest["display"] = "standalone";
            $globals->manifest["gcm_sender_id"] = "482941778795";

            $oPage->tplAddJs("OneSignalSDK", "OneSignalSDK.min.js", "/themes/site/javascript", false, false, null, false, "top");
            $js_content = 'var OneSignal = window.OneSignal || [];
                            OneSignal.push(function() {
                            OneSignal.init({
                              appId: "' . $actual_srv["code"] . '",
                                  notifyButton: {
                                    enable: false 
                                  },
                                  promptOptions: {
                                      actionMessage: "Attiva le notifiche e riceverai i consigli di salute del giorno in anteprima!",
                                      acceptButtonText: "No, Grazie",
                                      cancelButtonText: "Si, con piacere!" 
                                  }' .
                                (strlen($actual_srv["safari"]) ? ', safari_web_id: "' . $actual_srv["safari"] . '"' : "") . '
                                
                            });
                            var notificationPromptDelay = 3000;
                            var navigationStart = window.performance.timing.navigationStart;
                            var timeNow = Date.now();
                            setTimeout(promptAndSubscribeUser, Math.max(notificationPromptDelay - (timeNow - navigationStart), 0));
                          });
                          function promptAndSubscribeUser() {
                            window.OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                              if (!isEnabled) {
                                window.OneSignal.showSlidedownPrompt();
                              }
                            });
                          }';

            $oPage->tplAddJs("OneSignal", null, null, false, $oPage->isXHR(), $js_content, false, "bottom");
    }


}
