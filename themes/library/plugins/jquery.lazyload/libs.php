<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"lazyload" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.lazyload",
							"file" => "jquery.lazyload.js"
						)
					)
				)
			)
		)
	)
);
