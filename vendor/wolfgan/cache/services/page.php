<?php
/**
 * VGallery: CMS based on FormsFramework
 * Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @package VGallery
 *  @subpackage core
 *  @author Alessandro Stucchi <wolfgan@gmail.com>
 *  @copyright Copyright (c) 2004, Alessandro Stucchi
 *  @license http://opensource.org/licenses/gpl-3.0.html
 *  @link https://github.com/wolfgan43/vgallery
 */
class cachePage {
    const ENGINE                                            = "dynamic";
    const COMPRESS_TYPE                                     = "gz";
    const VALID_CACHE_START_AT                              = null;
    const TYPE                                              = "page";

    private $cache                                          = null;

    public function __construct($cache, $params = null)
    {
        //require_once(FF_DISK_PATH . "/library/gallery/system/cache.php");

        /**
         * Cache System
         */

        //$this->cache = check_static_cache_page();
        //print_r($this->cache);
    }

    public function get() {
        return $this->cache;
    }

    public function run($page, $request = null, $session = null) {
        require_once(FF_DISK_PATH . "/library/gallery/system/cache.php");

        /**
         * Cache System
         */

        $this->cache = check_static_cache_page();
        return;


        $path_info = $_SERVER["PATH_INFO"];

        $cache_params = $this->find($page, $request, $session);

        if(is_array($cache_params)) { //da verificare il nocache
            /*$schema = Cms::getSchema();

            $rule["path"] = $cache_params["user_path"];

            if($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest") {
                $rule["path"] = $cache_params["strip_path"] . $rule["path"];
            }
            $request_path = rtrim($cache_params["alias"] . $rule["path"], "/");
            if(!$request_path) {
                $request_path = "/";
            }
            $rule["split_path"] = explode("/", $request_path);
            if(isset($schema["request"][$request_path])) {
                $rule["match"] = $schema["request"][$request_path];
                //} elseif(0 && isset($schema["request"]["/" . $rule["split_path"][0]])) {
                //    $rule["match"] = $schema["request"]["/" . $rule["split_path"][0]];
            } elseif(isset($schema["request"][$rule["split_path"][count($rule["split_path"]) - 1]])) {
                $rule["match"] = $schema["request"][$rule["split_path"][count($rule["split_path"]) - 1]];
            } else {

                do {
                    $request_path = dirname($request_path);
                    if(isset($schema["request"][$request_path])) {
                        $rule["match"] = $schema["request"][$request_path];
                        break;
                    }
                } while($request_path != DIRECTORY_SEPARATOR); //todo: DS check
            }

            if($rule["match"]["ext"] && is_array($schema["request"][$rule["match"]["ext"]])) {
                $rule["match"] = array_merge_recursive($schema["request"][$rule["match"]["ext"]], $rule["match"]);
            }*/

            /*$request = cache_get_request($_GET, $_POST, $rule["match"]);
            if($request["nocache"]) {
                $cache_params = false;
            } elseif(isset($request["redirect"])) {
                $path_info = $_SERVER["PATH_INFO"];
                if($path_info == "/index") {
                    $path_info = "";
                }
                Cms::redirect($_SERVER["HTTP_HOST"] . $path_info . $request["valid"]);
            }
            //necessario XHR perche le request a servizi esterni path del domain alias non triggerano piu
            if(!$save_path && $cache_params["redirect"] && $_SERVER["HTTP_X_REQUESTED_WITH"] != "XMLHttpRequest") {// Evita pagine duplicate quando i link vengono gestiti dagli alias o altro
                Cms::redirect($cache_params["redirect"] . $request["valid"]);
            }*/
        }
        //Gestisce gli errori delle pagine provenienti da apachee con errorDocument nell'.htaccess
        if($cache_params === true) {
            $this->sender("error");
        }

        //se la cache e disabilitata
        if(!$cache_params) {
            if($cache_params === false) { //se page[exit] false disabilita l'uso della cache anche in scrittura
                //todo: probabilmente non serve
                define("DISABLE_CACHE", true);
            }
            return false;
        }

       // cache_check_lang($cache_params);
        $cache_file = cache_get_filename($cache_params, $request);

        if(!$save_path && $cache_file["invalid"]) {
            if($cache_file["is_error_document"] && !$cache_file["noexistfileerror"] && $cache_file["noexistfile"]) {
                $cache_file["invalid"] = false;
                $cache_file["filename"] = "index";
                if($cache_file["compress"]) {
                    $cache_file["primary"] = "index.gz";
                } else {
                    $cache_file["primary"] = "index.html";
                }
                $cache_file["gzip"] = "index.gz";
            } elseif($cache_file["noexistfile"]) {
                $arrSem[] = cache_sem($cache_file["cache_path"]);
                if (is_file($cache_file["cache_path"] . "/" . $cache_file["primary"])) {
                    $cache_file["invalid"] = false;
                } else {
                    $arrSem[] = cache_sem("create");
                }
            } elseif($cache_file["noexistfileerror"]) {
                $arrSem[] = cache_sem($cache_file["error_path"]);
            } elseif(!defined("DISABLE_CACHE")) {
                if(is_array($schema["priority"]) && array_search($path_info, $schema["priority"]) !== false) {
                    @touch($cache_file["cache_path"] . "/" . $cache_file["primary"], time() + 10); //evita il multi loading di pagine in cache
                    Cache::log($cache_file["cache_path"] . "/" . $cache_file["primary"] . "  " . filemtime($cache_file["cache_path"] . "/" . $cache_file["primary"]) . " => " . (time() + 10), "update_primary");
                } else {
                    $sem = cache_sem("update", true);
                    if($sem["acquired"]) {
                        @touch($cache_file["cache_path"] . "/" . $cache_file["primary"], time() + 10); //evita il multi loading di pagine in cache
                        Cache::log($cache_file["cache_path"] . "/" . $cache_file["primary"] . "  " . filemtime($cache_file["cache_path"] . "/" . $cache_file["primary"]) . " => " . (time() + 10), "update");
                    } else {
                        $cache_file["invalid"] = false;
                        Cache::log($cache_file["cache_path"] . "/" . $cache_file["primary"] . "  " . filemtime($cache_file["cache_path"] . "/" . $cache_file["primary"]) . " => " . (time() + 10), "update_queue");
                    }
                    $arrSem[] = $sem;
                }
            }
        }

        if($cache_file["invalid"]) {
            //define("DISABLE_CACHE", true);
            $cache_exit = true;
        } else {
            $ff_contents = cache_check_ff_contents($path_info, $cache_file["last_update"]);
            if($ff_contents["cache_invalid"]) {
                //define("DISABLE_CACHE", true);
                $cache_exit = true;
            }
        }
//set_time_limit(15);
        if($save_path) {
            // cache_sem_release($arrSem);
            return array(
                "file" => $cache_file
            , "user_path" => $path_info
            , "params" => $cache_params
            , "request" => $request
            , "ff_count" => $ff_contents["count"]
            , "sem" => &$arrSem
            );
        }

        if(defined("DEBUG_MODE") && isset($_REQUEST["__nocache__"])) {
            $_REQUEST["__CLEARCACHE__"] = true;
            define("DISABLE_CACHE", true);

            cache_sem_remove($cache_file["cache_path"]);
            cache_send_header_content(null, false, false, false);

            if($cache_file["error_path"])
                cache_set_error_document($cache_file["error_path"], $cache_params);

            if(is_file($cache_file["cache_path"] . "/" . $cache_file["primary"])) {
                if($cache_file["primary"] != $cache_file["gzip"])
                    @unlink($cache_file["cache_path"] . "/" . $cache_file["primary"]);

                @unlink($cache_file["cache_path"] . "/" . $cache_file["gzip"]);
            }
        }

        if($cache_exit) {
            $load = sys_getloadavg();
            if ($load[0] > 80) {
                cache_sem_release($arrSem);

                cache_send_header_content(null, false, false, false);
                cache_http_response_code(503);

                readfile(FF_DISK_PATH . "/themes/gallery/contents/error_cache.html");
                exit;
            } else {
                if(!count($arrSem))
                    $arrSem[] = cache_sem();

                return array(
                    "file" => $cache_file
                , "user_path" => $path_info
                , "params" => $cache_params
                , "request" => $request
                , "ff_count" => $ff_contents["count"]
                , "sem" => &$arrSem
                );

                //return false;
            }
        }

        cache_sem_release($arrSem);


        if(!defined("DISABLE_CACHE"))
        {
            if($cache_file["is_error_document"])
            {
                //redirect
                if(!defined("FF_DISK_PATH"))
                    define("FF_DISK_PATH", FF_DISK_PATH);

                //require_once(FF_DISK_PATH . "/conf/gallery/config/db.php");
                require_once(FF_DISK_PATH . "/library/gallery/system/gallery_redirect.php");

                system_gallery_redirect($path_info, $request["valid"]);
                if($status_code === null)
                {
                    cache_http_response_code($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"
                        ? 500
                        : 404
                    );
                }
            }

            cache_parse($cache_file, $cache_params["lang"], $cache_params["auth"], $request["get"]);
        }

    }

    private function find($page, $request = null, $user = null) {
        $engine = "engine_" . $this::ENGINE;

        $res = $this->$engine($page, $request, $user);

        return ($res
            ? $res
            : $page["exit"]
        );
    }


    private function sender($signal) {
        switch ($signal) {
            case "error":
                $this->sender_error();

            default:
        }
        exit;
    }


    private function sender_error() {
        ffMedia::sendHeaders(null, array(
            "cache" => "must-revalidate"
        ));

        if(Cache::isXHR()) {
            cache_http_response_code(500);
        } else {
            cache_http_response_code(404);
        }

        Logs::write(" URL: " . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] . " REFERER: " . $_SERVER["HTTP_REFERER"], "log_error_apache");
        exit;
    }

    /**
     * @param $page
     * @param null $user
     * @return array|bool
     */
    private function engine_dynamic($page, $request = null, $user = null) {
        $cache                          = false;
        if($page["cache"]) {
            $cache["invalid"]           = true;
            $cache["noexistfile"]       = true;
            $cache["noexistfileerror"]  = null;

            $cache["compress"]          = ($page["compress"] && isset($_SERVER["HTTP_ACCEPT_ENCODING"]) && strpos($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip") === false
                                            ? false
                                            : true
                                        );
            $cache["base_path"]         = $this->engine_dynamic_get_path(
                                            $page["cache"]
                                            , $page["lang"]
                                            , ($page["session"] === false
                                                ? null
                                                : $user
                                            )
                                            , $page["cache_path"]
                                        );
            $cache["storing_path"]      = $cache["base_path"]
                                            . $this->engine_dynamic_rule($page["rule"], $page["user_path"])
                                            . $page["user_path"];
            $cache["filename"]          = $this->engine_dynamic_filename($request, $page["cache_rnd"]);
            $cache["ext"]               = $this->engine_dynamic_extension($cache["compress"], $page["type"]);
            $cache["primary"]           = $cache["filename"] . "." . $cache["ext"];
            $cache["gzip"]              = $cache["filename"] . "." . $this::COMPRESS_TYPE;

            $cache_final                = FF_DISK_PATH . $cache["storing_path"] . "/" . $cache["primary"];
            if(is_file($cache_final)) {
                $cache["noexistfile"]   = false;

                $cache["last_update"]   = filemtime($cache_final);
                $cache["created"]       = filectime($cache_final);

                if($cache["last_update"] >= $cache["created"]) {
                    $cache["invalid"]   = false;
                }

                if($this::VALID_CACHE_START_AT && $this::VALID_CACHE_START_AT > $cache["created"]) {
                    $cache["invalid"]   = true;
                }
            }

            if($cache["invalid"] && $cache["noexistfile"]) {
                $arrUserPath = explode("/", $page["user_path"]);
                $cache["error_path"]    = $this->engine_dynamic_get_path(
                                            $page["cache"]
                                            , $page["lang"]
                                            , ($page["session"] === false
                                                ? null
                                                : $user
                                            )
                                            , "/error-document"
                                        );
                $cache_error_final = FF_DISK_PATH . $cache["error_path"] . "/". $arrUserPath[1] . ".php";


                $cache["noexistfileerror"] = !is_file($cache_error_final);
                $cache["is_error_document"]= $this->get_error_document($cache_error_final, $cache["storing_path"] . "/" . $cache["filename"]);
            }

            $cache["client"] = $page["cache_client"];
            $cache["type"] = $page["type"];


            print_r($cache);
            die();
        }

        return $cache;
    }

    private function engine_dynamic_get_path($type, $lang, $user = null, $base_path = null) {
        if($type === "guest") {
            $cache_path             = "/global";
        } else {
            if($user) {
                if($type === "user") {
                    $auth_path      = ($user["username_slug"]
                        ? $user["username_slug"]
                        : preg_replace("/[^a-z\-0-9]/i", "", $user["username"])
                    );
                    $cache_path     = "/private";
                } else {
                    $auth_path      = $user["acl_primary"];
                }
            }

            if(!$auth_path) {
                $auth_path          = "guests";
            }
            if(!$cache_path) {
                $cache_path         = "/public";
            }
        }

        if(strpos(strtolower($_SERVER["HTTP_HOST"]), "www.") === 0) {
            $domain_name            = substr($_SERVER["HTTP_HOST"], strpos($_SERVER["HTTP_HOST"], ".") + 1);
        } else {
            $domain_name            = $_SERVER["HTTP_HOST"];
        }

        if(strpos($domain_name, ":") !== false) {
            $domain_name            = substr($domain_name, 0, strpos($domain_name, ":"));
        }
        //cache_get_locale($page["user_path"], $domain_name, $user_permission);


        $cache_base                 = Cache::CACHE_PATH . "/" . $domain_name . "/" . $lang
            . ($base_path
                ? $base_path
                : $cache_path
            )
            . (!$auth_path || ($auth_path == "guests" && $base_path)
                ? ""
                : "/" . $auth_path
            );

        return $cache_base;
    }

    private function engine_dynamic_rule($rule, $path_info) {
        $cache_base_rule = "";
        if(is_array($rule) && count($rule)) {
            foreach($rule AS $compare_path => $precision) {
                if(strpos($path_info, $compare_path) !== false) {
                    $arrCacheSplit = explode($compare_path, $path_info, 2);
                    if($arrCacheSplit[1]) {
                        $cache_base_rule = "/" . substr(ltrim($arrCacheSplit[1], "/"), 0, $precision);
                        break;
                    }
                }
            }
        }

        return $cache_base_rule;
    }

    private function engine_dynamic_filename($request, $random = false) {
        $cache_filename = "index";

        if($random) {
            $cache_filename .= rand(1, $random);
        }

        if(is_array($request["valid"]) && count($request["valid"])) {
            $cache_filename .= "_" . str_replace(array("&", "="), array("_", "-"), http_build_query($request["valid"]));
        }

        $cache_filename = preg_replace("/[^A-Za-z0-9\-_]/", '', $cache_filename);
        if(Cache::isXHR()) {
            $cache_filename .= "_XHR";
        }

        return $cache_filename;
    }

    private function engine_dynamic_extension($compress = false, $page_type = null) {
        if($compress) {
            $cache_ext = $this::COMPRESS_TYPE;
        } else {
            $cache_file_type = $page_type;
            if($cache_file_type == "mixed") {
                if (Cache::isXHR()) {
                    $cache_file_type = "json";
                } else {
                    $cache_file_type = "html";
                }
            } elseif(!$cache_file_type) {
                $cache_file_type = "html";
            }
            $cache_ext = $cache_file_type;
        }

        return $cache_ext;
    }

    private function engine_static($page, $request = null, $user = null) {
        $cache                          = false;

        return $cache;
    }

    private function engine_eternal($page, $request = null, $user = null) {
        $cache                          = false;

        return $cache;
    }

    private function get_error_document($file, $key)
    {
        $fs = new Filemanager("php");

        $page = $fs->read($file, $key);

        return $page;
    }

    private function set_error_document($cache_error_path, $params)
    {
        if($params["user_path"] && $params["user_path"] != "/") {
            $arrUserPath = explode("/", $params["user_path"]);
            $errorDocumentFile = $cache_error_path . "/" . $arrUserPath[1]; // . ".php";
            $key = $params["user_path"];

            //require_once (FF_DISK_PATH . "/library/gallery/models/filemanager/Filemanager.php");
            $fs = new Filemanager("php");

            return $fs->delete($key, $errorDocumentFile, Filemanager::SEARCH_IN_VALUE);
        }
    }
}

