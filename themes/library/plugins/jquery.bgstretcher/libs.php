<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"bgstretcher" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.bgstretcher",
							"file" => "jquery.bgstretcher.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.bgstretcher",
									"file" => "jquery.bgstretcher.css",
								)
							)
						)
					)
				)
			)
		)
	)
);
