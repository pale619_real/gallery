<?php
// ----------------------------------------
//  		FRAMEWORK FORMS vAlpha
//		      PLUGIN DEFINITION (slug)
//			   by Samuele Diella
// ----------------------------------------

class ffWidget_slug extends ffCommon
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $template_file 	 = "ffWidget.html";
    
    public $class			= "ffWidget_slug";
    
    public $widget_deps	= array();
    public $js_deps = array(
                              "jquery" 			=> null
                        );
    public $css_deps 		= array();
    // PRIVATE VARS
    
    public $oPage			= null;
    public $source_path	= null;
    public $style_path		= null;

    public $tpl 				= null;

    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        $this->get_defaults();

        $this->oPage = array(&$oPage);

        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->style_path = $style_path;
    }

    public function prepare_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }
    }

    public function process($id, &$value, ffField_html &$Field)
    {
        if ($Field->parent !== null && strlen($Field->parent[0]->id)) {
            $tpl_id = $Field->parent[0]->id;
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
            $this->tpl[$tpl_id]->set_var("container", $Field->parent[0]->id . "_");
        } else {
            $tpl_id = "main";
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
        }

        $this->tpl[$tpl_id]->set_var("site_path", $Field->parent_page[0]->site_path);
        $this->tpl[$tpl_id]->set_var("theme", $Field->getTheme());

        if (strlen($Field->widget_path)) {
            $this->tpl[$tpl_id]->set_var("widget_path", $Field->widget_path);
        } else {
            $this->tpl[$tpl_id]->set_var("widget_path", "/themes/responsive/ff/ffField/widgets/slug");
        }
        
        $this->tpl[$tpl_id]->set_var("slug_field", str_replace("[", '\\\\[', str_replace("]", '\\\\]', $id)));
        if (strpos($id, "[") === false) {
            $this->tpl[$tpl_id]->set_var("title_field", $Field->slug_title_field);
        } else {
            $this->tpl[$tpl_id]->set_var("title_field", str_replace("[", '\\\\[', str_replace("]", '\\\\]', str_replace("[" . $Field->id . "]", "", $id) . "[" . $Field->slug_title_field . "]")));
        }
        $this->tpl[$tpl_id]->parse("SectBinding", true);
        return null;
    }
    
    public function get_component_headers($id)
    {
        if ($this->oPage !== null) { //code for ff.js
 //           $this->oPage[0]->tplAddJs("ff.ffField", "ffField.js", FF_THEME_DIR . "/library/ff");
//			$this->oPage[0]->tplAddJs("ff.ffField.slug", "slug.js", FF_THEME_DIR . "/responsive/ff/ffField/widgets/slug");
        }
        
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectHeaders", false);
    }

    public function get_component_footers($id)
    {
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectFooters", false);
    }

    public function process_headers()
    {
        if ($this->oPage !== null) { //code for ff.js
 //           $this->oPage[0]->tplAddJs("ff.ffField", "ffField.js", FF_THEME_DIR . "/library/ff");
//			$this->oPage[0]->tplAddJs("ff.ffField.slug", "slug.js", FF_THEME_DIR . "/responsive/ff/ffField/widgets/slug");
            
            //return;
        }

        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectHeaders", false);
    }
    
    public function process_footers()
    {
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectFooters", false);
    }
}
