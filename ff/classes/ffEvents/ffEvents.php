<?php
/**
 * Event Queue
 *
 * @package FormsFramework
 * @subpackage base
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */

/**
 * Questa classe rappresenta un "fantoccio" utilizzato per creare una coda eventi "neutra", cioè non associata a nessun oggetto.
 * Non ha contenuto in quanto sono sufficienti i contenuti di ffCommon, che eredita
 *
 * @package FormsFramework
 * @subpackage base
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */
class ffEvents extends ffCommon
{
    /**
     * Questa classe è volutamente lasciata vuota. Leggi la descrizione della classe
     */
}
