<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"selectboxit" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.selectboxit",
							"file" => "jquery.selectboxit.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.selectboxit",
									"file" => "jquery.selectboxit.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
