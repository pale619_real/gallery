<?php
// ----------------------------------------
//  		FRAMEWORK FORMS vAlpha
//		      PLUGIN DEFINITION (datepicker)
//			   by Samuele Diella
// ----------------------------------------

class ffWidget_datepicker extends ffCommon
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $template_file 	 = "ffWidget.html";
    
    public $class			= "ffWidget_datepicker";

    public $widget_deps	= array();
    public $js_deps = array(
                              "jquery" 			=> null
                            , "jquery.ui" 		=> null
/*							, "jquery.ui/i18n" 		=> "jquery.ui.datepicker-it.js"*/
                        );
    public $css_deps 		= array(/*
                              "jquery.ui.core"        => array(
                                      "file" => "jquery.ui.core.css"
                                    , "path" => null
                                    , "rel" => "jquery.ui"
                                ),
                              "jquery.ui.theme"        => array(
                                      "file" => "jquery.ui.theme.css"
                                    , "path" => null
                                    , "rel" => "jquery.ui"
                                ),
                              "jquery.ui.datepicker"        => array(
                                      "file" => "jquery.ui.datepicker.css"
                                    , "path" => null
                                    , "rel" => "jquery.ui"
                                )*/
                        );
    // PRIVATE VARS
    
    public $oPage			= null;
    public $source_path	= null;
    public $style_path		= null;
    public $theme			= null;

    public $tpl			= null;

    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        $this->get_defaults();

        $this->oPage = array(&$oPage);

        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->style_path = $style_path;
    }

    public function prepare_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }
    }

    public function process($id, &$value, ffField_html &$Field)
    {
        if ($Field->parent !== null && strlen($Field->parent[0]->id)) {
            $tpl_id = $Field->parent[0]->id;
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
            $this->tpl[$tpl_id]->set_var("container", $Field->parent[0]->id . "_");
        } else {
            $tpl_id = "main";
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
        }

        $this->tpl[$tpl_id]->set_var("site_path", $Field->parent_page[0]->site_path);

        /*if($this->theme !== null) {
            $theme = $this->theme;
        } else {    */
        $theme = $Field->getTheme();
        //}
        
        $this->tpl[$tpl_id]->set_var("theme", $theme);

        if (is_array($Field->framework_css["widget"]["colorpicker"])) {
            $this->framework_css = array_replace_recursive($this->framework_css, $Field->framework_css["widget"]["colorpicker"]);
        }
                
        if (strlen($Field->widget_path)) {
            $this->tpl[$tpl_id]->set_var("widget_path", $Field->widget_path);
        } else {
            if (strlen($Field->parent_page[0]->jquery_ui_force_theme !== null)) {
                $this->tpl[$tpl_id]->set_var("widget_path", FF_SITE_PATH . "/themes/library/jquery-ui.themes/" . $Field->parent_page[0]->jquery_ui_force_theme . "/images");
            } else {
                $this->tpl[$tpl_id]->set_var("widget_path", FF_SITE_PATH . "/themes/" . $theme . "/images/jquery-ui");
            }
        }
        $this->tpl[$tpl_id]->set_var("id", $id);
        $this->tpl[$tpl_id]->set_var("lang", strtolower(substr(FF_LOCALE, 0, -1)));

        $css_deps 		= array(
        );
        /*
        if($Field->get_app_type() == "DateTime")
        {
            $css_deps["jquery.ui.timepicker"] = array(
                      "file" => "timepicker-addon.css"
                    , "path" => FF_THEME_DIR . "/responsive/ff/ffField/widgets/datepicker"
                );

            $css_deps["jquery.ui.slider"] = array(
                  "file" => "jquery.ui.slider.css"
                , "path" => null
                , "rel" => "jquery.ui"
            );
        }
        if(is_array($css_deps) && count($css_deps))
        {
            foreach($css_deps AS $css_key => $css_value)
            {
                $rc = $Field->parent_page[0]->widgetResolveCss($css_key, $css_value, $Field->parent_page[0]);
                $this->tpl[$tpl_id]->set_var(preg_replace('/[^0-9a-zA-Z]+/', "", $css_key), $rc["path"] . "/" . $rc["file"]);
                $Field->parent_page[0]->tplAddCss(preg_replace('/[^0-9a-zA-Z]+/', "", $css_key), $rc["file"], $rc["path"], "stylesheet", "text/css", false, false, null, false, "bottom");
            }
        }*/
        
        if ($Field->contain_error && $Field->error_preserve) {
            $this->tpl[$tpl_id]->set_var("value", ffCommon_specialchars($value->ori_value));
        } else {
            if ($Field->base_type == "Timestamp" && $value->getValue("Timestamp") <= 0) {
                $this->tpl[$tpl_id]->set_var("value", "");
            } else {
                $this->tpl[$tpl_id]->set_var("value", ffCommon_specialchars($value->getValue($Field->get_app_type(), $Field->get_locale())));
            }
        }
        $this->tpl[$tpl_id]->set_var("properties", $Field->getProperties());
        
        if (strlen($Field->class)) {
            $this->tpl[$tpl_id]->set_var("class", $Field->class);
        } else {
            $this->tpl[$tpl_id]->set_var("class", $this->class);
        }

        $Field->framework_css["fixed_post_content"] = array(2);
        $Field->fixed_post_content = '<a href="javascript:void(0);" onclick="$(\'#' . $Field->parent[0]->id . "_" . $id . '\').datepicker(\'show\');" class="' . cm_getClassByFrameworkCss("calendar", "icon") . '"></a>';
        /*messo nel css */ //$Field->properties["style"] = "position: relative; z-index: 100000;"; //workground per far funzionare il datepicker dentro le dialog modali
            
        if ($Field->min_year) {
            $this->tpl[$tpl_id]->set_var("min_year", $Field->min_year);
        } else {
            $this->tpl[$tpl_id]->set_var("min_year", "10");
        }

        if ($Field->max_year) {
            $this->tpl[$tpl_id]->set_var("max_year", $Field->max_year);
        } else {
            $this->tpl[$tpl_id]->set_var("max_year", "2");
        }
        
        if ($Field->get_app_type() == "DateTime" || $Field->datepicker_force_datetime) {
            //$this->tpl[$tpl_id]->parse("SectDateTimeHeader", false);
            $this->tpl[$tpl_id]->parse("SectDateTime", false);
            $this->tpl[$tpl_id]->set_var("SectDate", "");
        } else {
            // $this->tpl[$tpl_id]->set_var("SectDateTimeHeader", "");
            $this->tpl[$tpl_id]->set_var("SectDateTime", "");
            $this->tpl[$tpl_id]->parse("SectDate", false);
        }
        $this->tpl[$tpl_id]->parse("SectBinding", true);
        return;
        //return $Field->fixed_pre_content . $this->tpl[$tpl_id]->rpparse("SectControl", FALSE) . $Field->fixed_post_content;
    }
    
    public function get_component_headers($id)
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddJs("jquery.ui.datepicker", "jquery.ui.datepicker-" . strtolower(substr(FF_LOCALE, 0, -1)) . ".js", FF_THEME_DIR . "/library/jquery.ui/i18n");
            $this->oPage[0]->tplAddJs("jquery.ui.timepicker", "timepicker-addon.js", FF_THEME_DIR . "/responsive/ff/ffField/widgets/datepicker");
        }

        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectHeaders", false);
    }

    public function get_component_footers($id)
    {
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectFooters", false);
    }

    public function process_headers()
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddJs("jquery.ui.datepicker", "jquery.ui.datepicker-" . strtolower(substr(FF_LOCALE, 0, -1)) . ".js", FF_THEME_DIR . "/library/jquery.ui/i18n");
            
            //return;
        }
        
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectHeaders", false);
    }
    
    public function process_footers()
    {
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectFooters", false);
    }
}
