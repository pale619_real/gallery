<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"crystalnotifications" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.crystalnotifications/js",
							"file" => "jquery.crystalnotifications.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.crystalnotifications/css",
									"file" => "jquery.crystalnotifications.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
