<?php
/**
 *   VGallery: CMS based on FormsFramework
Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * @package VGallery
 * @subpackage core
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
* @license http://opensource.org/licenses/lgpl-3.0.html
* @link https://bitbucket.org/cmsff/vgallery
 */

class Filemanager extends vgCommon
{
    private static $singleton                                           = null;
    private static $storage                                             = null;

    const SEARCH_IN_KEY                                                 = 1;
    const SEARCH_IN_VALUE                                               = 2;
    const SEARCH_IN_BOTH                                                = 3;
    const SCAN_DIR                                                      = 1;
    const SCAN_DIR_RECURSIVE                                            = 2;
    const SCAN_FILE                                                     = 4;
    const SCAN_FILE_RECURSIVE                                           = 8;
    const SCAN_ALL                                                      = 16;
    const SCAN_ALL_RECURSIVE                                            = 32;

    const SEARCH_DEFAULT                                                = Filemanager::SEARCH_IN_KEY;

    protected $services                                                 = array(                //servizi per la scrittura o lettura della notifica
        "fs"                                                            => null
    );
    protected $controllers                                              = array(
        "fs"                                                            => array(
            "default"                                                   => null
            , "services"                                                => null
        )
    );
    protected $controllers_rev                                          = null;
    protected $path                                                     = null;
    protected $var                                                      = null;
    protected $keys                                                     = null;
    protected $data                                                     = null;
    protected $expires                                                  = null;

    private $result                                                     = array();

    public static function preload($rules = null) {
        if($rules) {
            if(is_array($rules) && count($rules)) {
                foreach($rules AS $path => $params) {
                    if(is_dir(self::$disk_path . $path)) {
                        $directory = new RecursiveDirectoryIterator(self::$disk_path . $path, RecursiveDirectoryIterator::SKIP_DOTS);
                        $flattened = new RecursiveIteratorIterator($directory);

                        // Make sure the path does not contain "/.Trash*" folders and ends eith a .php
                        $files = new RegexIterator($flattened, $params["filter"]);

                        foreach ($files as $file) {
                            if (!self::setStorage($file, $params["rules"])) {
                                self::$storage["unknowns"][] = $file->getPathname();
                            }
                        }
                    }
                }
            }
        }

        return self::$storage;
    }

    private static function scanAddItem($file, $opt = null) {
        if(is_callable(self::$storage["scan"]["callback"])) {
            if($opt["filter"] && !$opt["filter"][pathinfo($file, PATHINFO_EXTENSION)]) {
                return;
            }

            $callback = self::$storage["scan"]["callback"];
            $callback($file, self::$storage);
        } elseif(!$opt) {
            self::$storage["scan"]["rawdata"][] = $file;
        } else {
            $file_info = pathinfo($file);
            if($opt["filter"] && !$opt["filter"][$file_info["extension"]]) {
                return;
            }

            if($opt["rules"] && !self::setStorage($file_info, $opt["rules"])) {
                self::$storage["unknowns"][] = $file;
            }
        }
    }

    private static function scanRun($pattern, $what = null) {
        $pattern = (strpos($pattern, self::$disk_path) === 0
                ? ""
                : self::$disk_path
            )
            . $pattern
            . (strpos($pattern, "*") === false
                ? '/*'
                : ''
            );

        if($what["filter"] && isset($what["filter"][0])) {
            $what["filter"] = array_fill_keys($what["filter"], true);
        }

        switch ($what) {
            case Filemanager::SCAN_DIR:
                if(self::$storage["scan"]["callback"]) {
                    self::glob_dir_callback($pattern);
                } else {
                    self::glob_dir($pattern);
                }
                break;
            case Filemanager::SCAN_DIR_RECURSIVE:
                self::rglob_dir($pattern);
                break;
            case Filemanager::SCAN_ALL:
                self::glob($pattern, false);
                break;
            case Filemanager::SCAN_ALL_RECURSIVE:
                self::rglobfilter($pattern, false);
                break;
            case Filemanager::SCAN_FILE:
                self::glob($pattern);
                break;
            case Filemanager::SCAN_FILE_RECURSIVE:
                self::rglobfilter($pattern);
                break;
            case null;
                self::rglob($pattern);
                break;
            default:
                self::rglobfilter($pattern, $what);
        }
    }

    private static function glob_dir($pattern) {
        self::$storage["scan"] = glob($pattern, GLOB_ONLYDIR);
    }
    private static function glob_dir_callback($pattern) {
        foreach(glob($pattern, GLOB_ONLYDIR) AS $file) {
            self::scanAddItem($file);
        }
    }
    private static function rglob_dir($pattern) {
        foreach(glob($pattern, GLOB_ONLYDIR) AS $file) {
            self::scanAddItem($file);
            self::rglob_dir($file . '/*');
        }
    }

    private static function glob($pattern, $opt = null) {
        if(is_array($opt["filter"])) {
            $flags = GLOB_BRACE;
            $limit = ".{" . implode(",", $opt["filter"]) . "}";
        }

        foreach(glob($pattern . $limit, $flags) AS $file) {
            if($opt === false || is_file($file)) {
                self::scanAddItem($file, $opt["rules"]);
            }
        }
    }
    private static function rglob($pattern) {
        foreach(glob($pattern) AS $file) {
            if(is_file($file)) {
                self::scanAddItem($file);
            } else {
                self::rglob($file . '/*');
            }
        }
    }

    private static function rglobfilter($pattern, $opt = null) {
        foreach(glob($pattern) AS $file) {
            if(is_file($file)) {
                self::scanAddItem($file, $opt);
            } else {
                if($opt === false) {
                    self::scanAddItem($file);
                }
                self::rglobfilter($file . '/*', $opt);
            }
        }
    }

/*
    private static function recursiveiterator($pattern, $filter = null) {
        if(is_array($filter)) {
            $limit = '\.(?:' . implode("|", $filter). ')';
        }

        $directory = new RecursiveDirectoryIterator($pattern, RecursiveDirectoryIterator::SKIP_DOTS);
        $flattened = new RecursiveIteratorIterator($directory);

        // Make sure the path does not contain "/.Trash*" folders and ends eith a .php
        $files = new RegexIterator($flattened, '#^(?:[A-Z]:)?(?:/(?!\.Trash)[^/]+)+/[^/]+' . $limit . '$#Di');

        foreach ($files as $file) {
            self::scanAddItem($file->getPathname());
        }
    }*/
/*
    private static function DirectoryIterator($pattern, $recursive = false) {
        foreach (new DirectoryIterator($pattern) as $fileInfo) {
            if($fileInfo->isDot()) { continue; }
            if($fileInfo->isDir()) {
                self::scanAddItem($fileInfo->getPathname());
                if($recursive) {
                    self::DirectoryIterator($fileInfo->getPathname(), true);
                }
            }
        }
    }
    */
/*
    private static function RecursiveDirectoryIterator($pattern) {
        foreach ($iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($pattern,
                RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST) as $fileInfo) {

            if($fileInfo->isDir()) {
                self::scanAddItem($fileInfo->getPathname());
            }
        }
    }*/
    /*
    private static function readdir($pattern, $recursive = false) {
        if (($handle = opendir($pattern))) {
            while (($file = readdir($handle)) !== false) {
                if (($file == '.') || ($file == '..')) { continue; }

                if (is_dir($pattern . "/" . $file)) {
                    self::scanAddItem($pattern . "/" . $file);
                    if($recursive) {
                        self::readdir($pattern . "/" . $file, true);
                    }
                }
            }
            closedir($handle);
        }
    }*/
    public static function scan($patterns, $what = null, $callback = null) {
        if(is_array($patterns) && !$callback) {
            $callback = $what;
        }

        self::$storage["scan"]   = array(
            "rawdata" => array()
            , "callback" => $callback
        );

        if(is_array($patterns) && count($patterns)) {
            foreach($patterns AS $pattern => $opt) {
                self::scanRun($pattern, $opt);
            }
        } else {
            self::scanRun($patterns, $what);
        }


        /**
         * find file
         */
        /*
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::glob(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms glob file";
//sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::rglob(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms glob file recursive";


        self::$storage["scan"] = array();
        $time = microtime(true);
        self::recursiveiterator(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms recursiveiterator file";
//sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::rglobfilter(FF_DISK_PATH, array("php" => true, "html" => true));
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms glob file recursive filtered";


        self::$storage["scan"] = array();
        $time = microtime(true);
        self::recursiveiterator(FF_DISK_PATH, array("php", "html"));
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms recursiveiterator file filtered";
//sleep(1);

*/


        /***
         * find dir
         */
/*
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::glob_dir(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms glob";
//sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::readdir(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms readdir";
// sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::DirectoryIterator(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms directoryiterator";

        echo "\n--------------------------------------\n";
        //sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::rglob_dir(FF_DISK_PATH);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms rglob recoursive";
// sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::readdir(FF_DISK_PATH, true);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms readdir recoursive";
// sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::DirectoryIterator(FF_DISK_PATH, true);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms directoryiterator recoursive";
// sleep(1);
        self::$storage["scan"] = array();
        $time = microtime(true);
        self::RecursiveDirectoryIterator(FF_DISK_PATH, true);
        //print_r(self::$storage["scan"]);
        echo "\n";
        $time = microtime(true) - $time;
        echo $time . "ms Recursivedirectoryiterator";
*/
        return self::$storage;
    }
    private static function setStorage($file_info, $rules) {
        if(is_array($rules) && count($rules)) {
            $key = $file_info["filename"];
            $file = $file_info["dirname"] . "/" . $file_info["basename"];

            foreach($rules AS $rule => $type) {
                if(strpos($file, $rule) !== false) {
                    self::$storage[$type][$key] = $file;
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * @param null $service
     * @param null $path
     * @param null $var
     * @return Filemanager|null
     */
    public static function getInstance($services = null, $path = null, $var = null)
    {
        if (self::$singleton === null) {
            self::$singleton                                            = new Filemanager($services, $path, $var);
        } else {
            self::$singleton->path                                      = $path;
            self::$singleton->var                                       = $var;
        }

        return self::$singleton;
    }

    /**
     * Filemanager constructor.
     * @param null $service
     * @param null $path
     * @param null $var
     */
    public function __construct($services = null, $path = null, $var = null)
    {
		$this->loadControllers(__DIR__);

    	$this->setServices(is_array($services)
            ? $services
            : array("fs" => $services)
        );

        $this->path                                                     = $path;
        $this->var                                                      = $var;
    }

    /**
     * @param null $path
     * @return bool
     */
    public function makeDir($path = null)
    {
        $rc = true;
        if(!$path)
			$path = dirname($this->path);

		if(!is_dir($path))
            $rc                                                         = @mkdir($path, 0777, ture);

        return $rc;
    }

    /**
     * @param null $keys
     * @param null $path
     * @param null $flags
     * @return array|bool|mixed|null
     */
    public function read($path = null, $keys = null, $flags = null)
    {
        $this->clearResult();
        if($this->isError())
            return $this->isError();
        else {
            $this->action                                               = "read";
            $this->keys                                                 = $keys;
            $this->controller($path, $flags);
        }

        return $this->getResult();
    }

    /**
     * @param null $data
     * @param null $var
     * @param null $expires
     * @param null $path
     * @return array|bool|mixed|null
     */
    public function write($data = null, $path = null, $var = null, $expires = null)
    {
        $this->clearResult();
        if($this->isError())
            return $this->isError();
        else {

            $this->action                                               = "write";
            $this->data                                                 = $data;
            $this->expires                                              = $expires;
            $this->controller($path, $var);
        }

        return $this->getResult();
    }

    /**
     * @param null $data
     * @param null $var
     * @param null $expires
     * @param null $path
     * @return array|bool|mixed|null
     */
    public function update($data = null, $path = null, $var = null, $expires = null)
    {
        $this->clearResult();
        if($this->isError())
            return $this->isError();
        else {
            $this->action                                               = "update";
            $this->data                                                 = $data;
            $this->expires                                              = $expires;
            $this->controller($path, $var);
        }

        return $this->getResult();
    }

    /**
     * @param null $keys
     * @param null $path
     * @param null $flags
     * @return array|bool|mixed|null
     */
    public function delete($keys = null, $path = null, $flags = null)
    {
        $this->clearResult();
        if($this->isError())
            return $this->isError();
        else {
			$this->action                                               = "delete";
            $this->keys                                                 = $keys;
            $this->controller($path, $flags);
        }

        return $this->getResult();
    }

    /**
     * @param $buffer
     * @param null $expires
     * @param null $path
     * @return bool|int
     */
    public function save($buffer, $expires = null, $path = null)
    {
		if(!$path)
    		$path = $this->getPath();

		$rc = $this->makeDir(dirname($path));
        if ($rc)
        {
            if ($rc = @file_put_contents($path, $buffer, LOCK_EX))
                @chmod($path, 0777);

            if ($rc && $expires !== null)
            {
                $this->touch($expires, $path);
            }
        }

		return $rc;
    }

    /**
     * @param $expires
     * @param null $path
     * @return bool
     */
    public function touch($expires, $path = null)
    {
    	if(!$path)
			$path = $this->getPath();

		$rc                                                             = @touch($path, $expires);
        //if (!$rc)
        //    @unlink($path);

        return $rc;
    }

    /**
     * @param null $path
     * @return bool
     */
    public function isExpired($path = null)
    {
    	if(!$path)
			$path = $this->getPath();

        return (filemtime($path) >= filectime($path)
            ? false
            : true
        );
    }

    /**
     * @param null $type
     * @return bool
     */
    public function exist($type = null) {
		$path = $this->getPath($type);

		return (is_file($path)
			? true
			: false
		);
	}


    /**
     * @param null $type
     * @param null $path
     * @return string
     */
    public function getPath($type = null, $path = null) {
		if(!$type) {
			$service = reset($this->services);
			$type = $service["default"];
		}
		if(!$path)
			$path = $this->path;

		if(!$path)
			$path = $this->path;

		return dirname($path) . "/" . basename($path, "." . $type) . "." . $type;
	}

    /**
     * @param null $path
     * @param null $flags
     */
    private function controller($path = null, $flags = null)
    {
        if($path)
            $this->path                                                = $path;

        foreach($this->services AS $controller => $services)
        {
            $this->isError("");

			$funcController = "controller_" . $controller;
            $this->$funcController((is_array($services)
                ? $services["service"]
                : $services
            ), $flags);

            if($this->action == "read" && $this->result)
                break;
        }
    }

    /**
     * @param null $service
     * @param null $flags
     */
    private function controller_fs($service = null, $flags = null)
    {
        if(!$service)
            $service                                                    = $this->services["fs"]["default"];

        if($service)
        {
            $type                                                       = "fs";
            $controller                                                 = "filemanager" . ucfirst($service);
            //require_once($this->getAbsPathPHP("/filemanager/services/" . $type . "_" . $service, true));

            $driver                                                     = new $controller($this);
            //$db                                                         = $driver->getDevice();
           // $config                                                     = $driver->getConfig();


            if(!$this->isError()) {
                switch($this->action)
                {
                    case "read":
                        $this->result = $driver->read($this->keys, $flags);
                        break;
                    case "update":
                        $this->result = $driver->update($this->data, $flags);
                        break;
                    case "write":
                        $this->result = $driver->write($this->data, $flags);
                        break;
                    case "delete":
                        $this->result = $driver->delete($this->keys, $flags);
                        break;
                    default:
                }
            }


        }


    }

    /**
     *
     */
    private function clearResult()
    {
        $this->keys                                                     = null;
        $this->data                                                     = null;
        $this->expires                                                  = null;
        $this->result                                                   = array();

        $this->isError("");
    }

    /**
     * @return array|bool|mixed|null
     */
	private function getResult($service = null)
	{
		return ($this->isError()
			? $this->isError()
			: ($service
				? $this->result[$service]
				: $this->result
			)
		);
	}

    /*private function getResult()
    {
        return ($this->isError()
            ? false
            : ($this->result
                ? (is_array($this->keys) || count($this->result) > 1
                    ? $this->result
                    : array_shift($this->result)
                )
                : null
            )
        );
    }*/
}