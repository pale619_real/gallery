if(!hCore) var hCore = {
    auth : {}
};

jQuery.extend(true, hCore.auth, {
    login: function (url, selector, elem, redirect) {
        var selectorID = (selector
            ? "#" + selector
            : document
        );

        console.log(redirect);

        var domain = jQuery(selectorID).find("INPUT[name='domain']").val() || window.location.host;
        var username = jQuery(selectorID).find("INPUT[name='username']").val() || undefined;
        var password = jQuery(selectorID).find("INPUT[name='password']").val() || undefined;
        var token = jQuery(selectorID).find("INPUT[name='csrf']").val() || "";
        var stayConnect = jQuery(selectorID).find("INPUT[name='stayconnected-new']").is(':checked') || false;
        var redirect = redirect || ff.getURLParameter("redirect");

        console.log(redirect);

        if(redirect == 'null') {
            if(window.location.pathname != '/') {
                redirect = window.location.pathname;
            } else {
                redirect = "/";
            }
        }
        console.log(redirect);



        if (!url)
            url = '/login';

        if (!jQuery(selectorID + " .error-container").length) {
            jQuery(selectorID).prepend('<div class="error-container" />');
        }

        $.ajax({
            url: url,
            headers: {
                "domain": domain
                , "csrf": token
                , "refresh": stayConnect
            },
            method: 'POST',
            dataType: 'json',
            data: {
                "username": username
                , "password": password
                , "redirect": redirect
            }
        })
            .done(function (data) {
                console.log(window.location);
                if (data.status == "0") {
                    if (data.welcome) {
                        if (data.welcome.css.length) {
                            var style = document.createElement("style");
                            style.innerHTML = data.welcome.css;
                            document.head.appendChild(style);
                        }
                        if (data.welcome.js.length) {
                            var script = document.createElement("script");
                            script.innerHTML = data.welcome.js;
                            document.head.appendChild(script);
                        }
                        if (data.welcome.html.length) {
                            window.location.href = data.redirect

                            jQuery(".login-page-doctor.doctor-action").remove();
                            jQuery(selectorID + " .login-inner-container").html(data.welcome.html);
                        }
                    } else {
                        window.location.href = data.redirect;
                    }
                } else {
                    jQuery(elem).find(".disabled").css({
                        'opacity': '',
                        'pointer-events': ''
                    }).removeClass("disabled");
                    if (jQuery(selectorID + " .error-container").length) {
                        jQuery(selectorID + " .error-container").html('<div class="callout alert">' + data.error + '</div>');
                    }
                }
            })
            .fail(function (data) {
                jQuery(elem).find(".disabled").css({
                    'opacity': '',
                    'pointer-events': ''
                }).removeClass("disabled");
                if (jQuery(selectorID + " .error-container").length) {
                    jQuery(selectorID + " .error-container").html('<div class="callout alert">' + 'Service Temporary not Available. Try Later.' + '</div>');
                }
            });

        return false;
    },
    social: function (title, url) {
        var social_window = undefined;
        social_window = window.open(
            url
            , title
            , "menubar=no, status=no, height=500, width= 500"
        );
    },
    submitProcessKey : function(e, button) {
        if (null == e)
            e = window.event;
        if (e.keyCode === 13)  {
            document.getElementById(button).focus();
            document.getElementById(button).click();
            return false;
        }
    }
});