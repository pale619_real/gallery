if(!hCore) var hCore = {
    auth : {}
};

jQuery.extend(true, hCore.auth, {
    bExpire : 300000,
    bearer : "",
    expire: "",
    recover : function (url, action, selector, elem, resendCode, type) {
        var selectorID = (selector
                ? "#" + selector
                : document
        );

        var error = "";
        var domain = jQuery(selectorID).find("INPUT[name='domain']").val() || undefined;
        var email = jQuery(selectorID).find("INPUT[name='user-email']").val() || undefined;
        var token = jQuery(selectorID).find("INPUT[name='csrf']").val() || "";
        var verifyCode = jQuery(selectorID).find("INPUT[name='codice-conferma']").val() || "";
        var password = jQuery(selectorID).find("INPUT[name='password']").val() || "";
        var confirmPassword = jQuery(selectorID).find("INPUT[name='confirm-password']").val() || "";
        var redirect = jQuery(selectorID).find("INPUT[name='redirect']").val() || "";
        var referral = jQuery(selectorID).find("INPUT[name='referral']").val() || "";

        var resendUrl = "hCore.auth.recover('" + url + "', '" + action + "', '" + selector + "', '" + elem + "', true);";
        var resendLink = '<a onclick="' + resendUrl + '" style="color:#009fe3;cursor:pointer;">Manda un nuovo codice</a>';
        if(resendCode) {
            jQuery(selectorID).find("INPUT[name='codice-conferma']").val('');
            jQuery(selectorID).find("INPUT[name='password']").val('');
            jQuery(selectorID).find("INPUT[name='confirm-password']").val('');
            hCore.auth.setBearer();
        }

        var bearer = jQuery(selectorID).find("INPUT[name='bearer']").val() || "";
        if(bearer) {
            this.setBearer(bearer);
        }

        var data = {};
        var headers = {};

        if(!action)
            action = (!ff.group ? "login" : "logout");

        if(!url)
            url = '/recover';

        if(this.expire <= new Date().getTime()) {
            this.bearer = "";
            this.expire = "";
        }

        if(verifyCode.length) {
            if(!password.length) {
                error = 'Non è stata indicata una nuova password';
            }
            if(!error.length && !confirmPassword.length) {
                error = 'Non è stato compilato il campo "conferma password"';
            }
            if(!error.length && confirmPassword != password) {
                error = 'I campi "password" e "conferma password" non coincidono';
            }

            if(!error.length) {
                headers = {
                    "Bearer": this.bearer
                    , "domain": domain
                    , "csrf": token
                };
                data = {
                    "t": $.trim(verifyCode)
                    , "key": password
                    , "scopes": "password"
                    , "redirect": redirect
                    , "username": email
                };
            } else {
                jQuery(selectorID + " .error-container").html('<div class="callout alert">' + error + '</div>');
            }
        } else {
            headers = {
                "domain": domain
                , "csrf": token
            };
            data = {
                "username": email
                , "scopes" : "password"
                , "type": type
                , "referral": referral
            };
        }

        if(!jQuery.isEmptyObject(headers) && !jQuery.isEmptyObject(data)) {
            jQuery(selectorID + " .error-container").html('');
            jQuery(elem).css({
                'opacity': '0.5'
            }).attr("disabled", "disabled");

            $.ajax({
                url: url,
                headers: headers,
                method: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    jQuery(elem).css({
                        'opacity': ''
                    }).removeClass("disabled");

                    if (data.status === "0") {
                        if(data["redirect"] !== undefined) {
                            $(selectorID + " .error-container").html('<div class="callout success">' + 'Operazione completata con successo!' + '</div>');

                            if(data["redirect"].length) {
                                window.location.href = data["redirect"];
                            } else {
                                window.location.href = "/user";
                            }
                        } else {
                            hCore.auth.setBearer(data["t"]);
                            if(!data["sender"]) {
                                data["sender"] = "email";
                            }

                            if (data["sender"]) {
                                $(selectorID + " .conferma-email-title").html("Modifica Password");
                                if(window.location.hostname == "pro.paginemediche.it") {
                                    $(selectorID + " .conferma-email-subtitle").html('<div style="font-size:14px">Controlla la tua casella di posta. Ti abbiamo appena inviato una mail con un codice che dovrai inserire nel campo sottostante per impostare una nuova password. Ricordati che la tua password è personale e che grazie ad essa è possibile accedere a informazioni sensibili. Tienila al sicuro.<br><br>Per rispondere ai requisiti di sicurezza la tua password deve:<ul style="list-style-type: square;"><li>essere di almeno 8 caratteri</li><li>contenere almeno una lettera maiuscola e una minuscola</li><li>contenere almeno un numero</li></ul></div>');
                                } else {
                                    $(selectorID + " .conferma-email-subtitle").html('<div style="font-size:14px">Controlla la tua casella di posta. Ti abbiamo appena inviato una mail con un codice che dovrai inserire nel campo sottostante per impostare una nuova password. Ricordati che la tua password è personale e che grazie ad essa è possibile accedere ai tuoi dati salute nella tua Area Personale!<br><br>Per rispondere ai requisiti di sicurezza la tua password deve:<ul style="list-style-type: square;"><li>essere di almeno 8 caratteri</li><li>contenere almeno una lettera maiuscola e una minuscola</li><li>contenere almeno un numero</li></ul></div>');
                                }
                                $(selectorID + " .error-container").html('<div class="callout success">' + 'Controlla la tua ' + data["sender"] + '</div>');
                                jQuery(selectorID).find(".hide-code-string").removeClass("hide-code-string");
                            }
                        }
                    } else {
                        if (jQuery(selectorID + " .error-container").length) {
                            jQuery(selectorID + " .error-container").html('<div class="callout alert">' + data.error + (data.status === "404" ? " " + resendLink : "") + '</div>');
                        }
                    }
                }
            });
        }

        return false;
    },
    "submitProcessKey" : function (elem) {
        var e = window.event;
        if (e.keyCode == 13)  {
            $(elem).next().focus();
            $(elem).next().click();
            return false;
        }
    },
    "setBearer" : function(t) {
        if(t) {
            this.bearer = t;
            this.expire = new Date().getTime() + this.bExpire;
        } else {
            this.bearer = "";
            this.expire = "";
        }
    }
});