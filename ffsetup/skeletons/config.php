<?php
/**
 * framework config example file
 *
 * @package FormsFramework
 * @subpackage common
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */

// *****************
//  GLOBAL SETTINGS
// *****************

define("FF_ENV_DEVELOPMENT", "{FF_ENV_DEVELOPMENT}");
define("FF_ENV_STAGING", "not_set");
define("FF_ENV_PRODUCTION", "not_set");

switch (true)
{
	case (substr($_SERVER["HTTP_HOST"], (strlen(FF_ENV_DEVELOPMENT) * -1)) == FF_ENV_DEVELOPMENT): 
		// PATH SETTINGS
		define("FF_DISK_PATH", "{FF_DISK_PATH}");
		define("FF_SITE_PATH", "{FF_SITE_PATH}");

		// DEFAULT DB CONNECTION FOR ffDb_Sql
		define("FF_DATABASE_HOST", "{FF_DATABASE_HOST}");
		define("FF_DATABASE_NAME", "{FF_DATABASE_NAME}");
		define("FF_DATABASE_USER", "{FF_DATABASE_USER}");
		define("FF_DATABASE_PASSWORD", "{FF_DATABASE_PASSWORD}");
		
		define("FF_ENV", FF_ENV_DEVELOPMENT);
		
		define("FF_ENABLE_MEM_TPL_CACHING", {FF_ENABLE_MEM_TPL_CACHING});
		define("FF_ENABLE_MEM_PAGE_CACHING", {FF_ENABLE_MEM_PAGE_CACHING});
		break;
		
	case (substr($_SERVER["HTTP_HOST"], (strlen(FF_ENV_STAGING) * -1)) == FF_ENV_STAGING):
		// PATH SETTINGS
		/**#@+
		 * @ignore
		 */
		define("FF_DISK_PATH", "");
		define("FF_SITE_PATH", "");

		// DEFAULT DB CONNECTION 
		define("FF_DATABASE_HOST", "");
		define("FF_DATABASE_NAME", "");
		define("FF_DATABASE_USER", "");
		define("FF_DATABASE_PASSWORD", "");

		define("FF_ENV", FF_ENV_STAGING);
		break;
		
	case (substr($_SERVER["HTTP_HOST"], (strlen(FF_ENV_PRODUCTION) * -1)) == FF_ENV_PRODUCTION):
		// PATH SETTINGS
		define("FF_DISK_PATH", "");
		define("FF_SITE_PATH", "");

		// DEFAULT DB CONNECTION 
		define("FF_DATABASE_HOST", "");
		define("FF_DATABASE_NAME", "");
		define("FF_DATABASE_USER", "");
		define("FF_DATABASE_PASSWORD", "");

		define("FF_ENV", FF_ENV_PRODUCTION);
		/**#@-*/
		break;
}

// unique application id
define("APPID", "{APPID}");

// session name
session_name("{session_name}");

<!--BeginSectFF_DEFAULT_THEME-->
define("FF_DEFAULT_THEME", "{FF_DEFAULT_THEME}");<!--EndSectFF_DEFAULT_THEME-->

// activecomboex
$plgCfg_ActiveComboEX_UseOwnSession = false;	/* set to true to bypass session check.
													NB: ActiveComboEX require a session. If you disable session
														check, ActiveComboEX do a session_start() by itself. */

/* DEFAULT FORMS SETTINGS
	this is a default array used by Forms classes to set user defined global default settings.
	the format is:
		$ff_global_setting[class_name][parameter_name] = value;
 */

// ****************
//  ERROR HANDLING
// ****************

// used to bypass certain ini settings
ini_set("display_errors", true);

/* used to define errors handled by internal error function.
   NB:
   Is not safe to use errors different from E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE
 */
define("FF_ERRORS_HANDLED", E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE);

/* used to define errors handled by PHP. 
   NB:
   This will be bit-masquered with FF_ERRORS_HANDLED by the framework.
 */
error_reporting((E_ALL ^ E_NOTICE) | E_STRICT);

/* used to define maximum recursion when digging into arrays/objects. NULL mean no limit. */
define("FF_ERRORS_MAXRECURSION", NULL);

// ***************
//  FILE HANDLING
// ***************

// disable file umasking
@umask(0);

// **********************
//  INTERNAZIONALIZATION
// **********************

// default data type conversion
define("FF_LOCALE", "ITA");
define("FF_SYSTEM_LOCALE", "ISO9075"); /* this is the locale setting used to convert system data, like url parameters.
											 this not affect the user directly. */
											 
date_default_timezone_set("Europe/Rome");

define("FF_DEFAULT_CHARSET", "UTF-8");

// **********************
//  FEATURES
// **********************

// better define above, in the hosts section
//define("FF_ENABLE_MEM_TPL_CACHING", false);
//define("FF_ENABLE_MEM_PAGE_CACHING", false);
define("FF_CACHE_DEFAULT_TBLREL", false);
define("FF_DB_INTERFACE", "mysqli");
define("FF_ORM_ENABLE", true);