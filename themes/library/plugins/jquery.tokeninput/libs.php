<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"tokeninput" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.tokeninput",
							"file" => "jquery.tokeninput.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.tokeninput",
									"file" => "token-input.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
