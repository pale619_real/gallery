<?php
/**
 *   VGallery: CMS based on FormsFramework
 * Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * @package VGallery
 * @subpackage core
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
 * @license http://opensource.org/licenses/lgpl-3.0.html
 * @link https://bitbucket.org/cmsff/vgallery
 */
$config_default = array();
$config = array_replace_recursive($config_default, (array)$config);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["username"])) {
        $type = "utente";
        cmCache_writeLog(array("created" => time(), "host" => $_SERVER["HTTP_HOST"], "username" => $_POST["username"], "POST" => $_POST), "recover");
        if (isset($_POST["t"])) {
            if (checkPassword($_POST["key"])) {
                $response = array(
                    "status" => "400"
                    , "error" => "La password selezionata non rispetta i criteri di sicurezza."
                );
            } else {
                $response = null;
                $secret = "password" . "-" . $_POST["username"] . '-' . $_POST["t"];
                $sid = Auth::getInstance("token")->create($secret);
                $return = checkSid($_POST["username"], $sid, "password", $type);

                if ($return["result"] == "success") {
                    $token = getTokenLocalDB($return["user"]["ID"]);
                    $response = array(
                        "status" => "0"
                    , "redirect" => true
                    , "error" => ""
                    );
                    $response = array_replace($response, (array)call_user_func_array($config["callback"]["recover"], array($response)));

                    if (strlen($token)) {
                        $auth = Auth::getInstance("session")->create($token);
                        updateAccessInfo("password", $_POST["username"], $_POST["key"], $type);
                        updateUserInfo("activate", null, $token);

                        if ($_REQUEST["redirect"]) {
                            $response["redirect"] = $_REQUEST["redirect"];
                        }
                    } else {
                        $response["redirect"] = "/login";
                    }
                } else {
                    $response = array(
                        "status" => "400"
                    , "error" => "Errore durante il controllo del codice di sicurezza."
                    );
                }
            }

            if (!$response) {
                $response = array(
                    "status" => "400"
                , "error" => "Error during password recover"
                );
            }
        } else {
            $response = Auth::request($_POST["username"], array("scopes" => "password", "type" => $_POST["type"], "referral" => $_POST["referral"]));

            if (strlen($response["code"])) {
                $secret = "password" . "-" . $_POST["username"] . '-' . $response["code"];
                $sid = Auth::getInstance("token")->create($secret);
                $return = setSid($_POST["username"], $sid, $response["code"], "password", $type);

                if ($return["result"] == "success") {
                    $response["code"] = "";
                    $response["status"] = "0";
                    $response["t"] = Auth::getInstance("token")->create();
                } else {
                    $response["status"] = "400";
                    $response["error"] = $return["message"];

                }

            } else {
                    $response["status"] = "400";

                }
        }
    }
    Api::send($response);
} else {

    $path = Auth::_getDiskPath("tpl") . ($config["tpl_path"]
            ? $config["tpl_path"]
            : "/recover"
        );
    $html_name = "/index.html";
    $css_name = "/style.css";
    $script_name = "/script.js";

    if (!is_file($file)) {
        $file = __DIR__ . $html_name;
    }

    $filename = (is_file($path . $html_name)
        ? $path . $html_name
        : __DIR__ . $html_name
    );
    $tpl = ffTemplate::factory(ffCommon_dirname($filename));
    $tpl->load_file(basename($filename), "main");

    $token = Auth::password();
    $tpl->set_var("csrf_token", $token);

    $tpl->set_var("recover_url", $config["api"]["recover"]);

    if ($config["domain"]) {
        $tpl->parse("SezDomain", false);
    } else {
        $tpl->set_var("domain_name", $_SERVER["HTTP_HOST"]);
        $tpl->parse("SezDomainHidden", false);
    }

    if ($_REQUEST["referer"]) {
        $tpl->set_var("referer_name", $_REQUEST["referer"]);
        $tpl->parse("SezReferer", false);
    }

    if (isset($_REQUEST["email"]) && strlen($_REQUEST["email"])) {
        /*
        $response = Auth::request($_REQUEST["email"], array("scopes" => "password"));

        $tpl->set_var("email_conferma", $_REQUEST["email"]);
        $tpl->set_var("email_class", "");

        $tpl->set_var("bearer_code", $response["t"]);
        $tpl->parse("SezBearerContainer", false);

        $tpl->set_var("recover_conferma_title","Modifica Password");
        $tpl->set_var("recover_conferma_subtitle",ffTemplate::_get_word_by_code("recover_subtitle_" . str_replace("-", "_", ffcommon_url_rewrite($_SERVER["HTTP_HOST"])) . "_step_2"));
        */
        Cms::redirect($_SERVER["PATH_INFO"]);
    } else {
        $tpl->set_var("email_class", "hide-code-string");
        $tpl->set_var("recover_conferma_title", "Hai dimenticato la password");
        $tpl->set_var("recover_conferma_subtitle", ffTemplate::_get_word_by_code("recover_subtitle_" . str_replace("-", "_", ffcommon_url_rewrite($_SERVER["HTTP_HOST"]))));
    }

    if (isset($_REQUEST["redirect"]) && strlen($_REQUEST["redirect"])) {
        $tpl->set_var("redirect", $_REQUEST["redirect"]);
    }

    $html = $tpl->rpparse("main", false);

    $css = file_get_contents(ffMedia::getFileOptimized(is_file($path . $css_name)
        ? $path . $css_name
        : __DIR__ . $css_name
    ));
    $js = file_get_contents(ffMedia::getFileOptimized(is_file($path . $script_name)
        ? $path . $script_name
        : __DIR__ . $script_name
    ));

    $output = array(
        "html" => $html
    , "css" => $css
    , "js" => $js
    );

    if (Auth::isXHR()) {
        Api::send($output);
    }
}

return $output;

function checkPassword($pwd)
{
    $error = false;

    if (strlen($pwd) < 8) {
        $error = true;
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $error = true;
    }

    if (!preg_match("#[a-z]+#", $pwd)) {
        $error = true;
    }

    if (!preg_match("#[A-Z]+#", $pwd)) {
        $error = true;
    }

    return $error;
}
