<?php

class ffWidget_disclosures extends ffCommon
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $template_file 	 = "ffWidget.html";

    public $class			= "ffWidget_disclosures";

    public $widget_deps	= array();
    public $js_deps = array(
                              "jquery" 			=> null
                            /*, "jquery.ui" 		=> null*/
                        );
    public $css_deps 		= array();
    /*var $css_deps 		= array(
                              "jquery.ui"		=> array(
                                      "file" => "jquery.ui.all.css"
                                    , "path" => null
                                )
                        );*/
    // PRIVATE VARS
    public $oPage			= null;
    public $source_path	= null;
    public $style_path		= null;

    public $tpl 			= null;

    public $processed_id	= array();

    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        $this->get_defaults();

        $this->oPage = array(&$oPage);

        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->style_path = $style_path;

        ffGrid::addEvent("on_tplParse", "ffWidget_disclosures::processButton", null, 0, null, null, array($this));
        ffRecord::addEvent("on_tplParse", "ffWidget_disclosures::processButton", null, 0, null, null, array($this));
        ffDetails::addEvent("on_tplParse", "ffWidget_disclosures::processButton", null, 0, null, null, array($this));
    }

    public function prepare_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("site_path", $this->oPage[0]->site_path);

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }
    }

    public function process($id, &$data, ffPage_base &$oPage)
    {
    }

    public function get_component_headers($id)
    {
        if ($this->oPage !== null) {//code for ff.js
            $this->oPage[0]->tplAddJs("ff.ffPage.disclosures", "disclosures.js", FF_THEME_DIR . "/responsive/ff/ffPage/widgets/disclosures");
        }

        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectHeaders", false);
    }

    public function get_component_footers($id)
    {
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectFooters", false);
    }
    
    public function process_headers()
    {
        if ($this->oPage !== null) {//code for ff.js
            $this->oPage[0]->tplAddJs("ff.ffPage.disclosures", "disclosures.js", FF_THEME_DIR . "/responsive/ff/ffPage/widgets/disclosures");
            
            //return;
        }

        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectHeaders", false);
    }

    public function process_footers()
    {
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectFooters", false);
    }



    public static function processButton($obj, $tpl, $widget)
    {
        //ffErrorHandler::raise("asd", E_USER_ERROR, NULL, get_defined_vars());
        if ($obj->widget_discl_enable) {
            if (!isset($widget->tpl[$obj->id])) {
                $widget->prepare_template($obj->id);
            }
            $tpl->parse("SectDisclosures", false);

            $widget->tpl[$obj->id]->set_var("element", $obj->id . "_discl");
            $widget->tpl[$obj->id]->set_var("section", $obj->id . "_discl_sect");
            if ($obj->widget_def_open) {
                $widget->tpl[$obj->id]->set_var("state", "opened");
            } else {
                $widget->tpl[$obj->id]->set_var("state", "closed");
            }

            $widget->tpl[$obj->id]->set_var("component_id", $obj->id);
            $tpl->set_var("discl_bt", $widget->tpl[$obj->id]->rpparse("SectIstance", false));
            $tpl->set_var("content_wrap_start", '<div id="' . $obj->id . '_discl_sect">');
            $tpl->set_var("content_wrap_end", '</div>');

            $widget->tpl[$obj->id]->parse("SectInitElement", true);

            if (is_subclass_of($obj, "ffGrid_base")) {
                $tpl->parse("SectTitle", false);
            } elseif (is_subclass_of($obj, "ffRecord_base")) {
                $tpl->parse("SectTitle", false);
            }
        } else {
            $tpl->set_var("SectDisclosures", "");
        }
    }
}
