<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"stars" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.stars",
							"file" => "jquery.stars.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.stars",
									"file" => "jquery.stars.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
