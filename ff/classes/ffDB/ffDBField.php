<?php
/**
 * Abstract Data Field Rapresentation Class File
 *
 * @ignore
 * @package FormsFramework
 * @subpackage Abstract Data Field Rapresentation Class
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright &copy; 2004-2007, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */

/**
 * ffDBField è la classe preposta alla rappresentazione astratta dei campi utilizzati per la gestione dei dati
 *
 * @ignore
 * @package FormsFramework
 * @subpackage Abstract Data Field Rapresentation Class
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright &copy; 2004-2007, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */
class ffDBField extends ffCommon
{
    public $sName		= "";
    public $iOffset 	= null;

    public $eBaseType;
    
    public $bAllowNull;

    public $bKey;
    public $bPrimary;
    public $bUnique;
    
    public $bAutoInc;
    
    public $iMinLenght;
    public $iMaxLenght;
    
    public $bAllowNumeric;
    public $bAllowAlpha;
    public $bAllowSymbols;
    
    public $bAllowSigned;
    public $bAllowFloat;
    
    public $pParent;
    
    public $sComment;
    public $sTable;

    // SPECIFIC DATA CONNECTION OPTIONS
    
    public function __construct($sName)
    {
        $this->get_defaults();
        
        if (!strlen($sName)) {
            ffErrorHandler::raise("u must enter a valid name for ffDBField objects", E_USER_ERROR, $this, get_defined_vars());
        }
            
        $this->sName = $sName;
    }
}
