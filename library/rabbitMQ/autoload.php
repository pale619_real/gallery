<?php

spl_autoload_register(function ($class) {
    $prefix = 'PhpAmqpLib\\';
    $base_dir = __DIR__;


    // does the class use the namespace prefix?
    $len = strlen($prefix);
    $relative_class = substr($class, $len - 1);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . '/PhpAmqpLib' . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it

    if (file_exists($file)) {
        require $file;
    }
});
