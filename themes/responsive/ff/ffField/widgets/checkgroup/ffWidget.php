<?php
// ----------------------------------------
//  		FRAMEWORK FORMS vBeta
//		      PLUGIN DEFINITION (checkgroup)
//			   by Samuele Diella
// ----------------------------------------

class ffWidget_checkgroup
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)
    public $template_file 	 = "ffWidget.html";
    
    public $class			= null;

    public $widget_deps	= array();
    public $js_deps 		= array();
    public $css_deps 		= array();

    // PRIVATE VARS
    
    public $tpl 			= null;
    public $db				= null;

    public $oPage 			= null;
    public $source_path	= null;
    public $style_path 	= null;
    
    
    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        //$this->get_defaults();

        $this->oPage = array(&$oPage);
        
        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->style_path = $style_path;
        
        $this->db[0] = ffDb_Sql::factory();
    }

    public function prepare_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }
    }
    
    public function process($id, &$value, ffField_base &$Field)
    {
        // DO SOME CHECK..
        switch ($Field->base_type) {
            case "Text":
                switch ($Field->grouping_action) {
                    case "concat":
                        if ($Field->grouping_separator === null || !strlen($Field->grouping_separator)) {
                            ffErrorHandler::raise("Invalid Grouping Separator with Grouping Action 'concat'", E_USER_ERROR, $this, get_defined_vars());
                        }
                        if (is_array($Field->recordset)) {
                            foreach ($Field->recordset as $tmp_key => $tmp_value) {
                                if (strpos($tmp_value[0]->getValue(), $Field->grouping_separator) !== false) {
                                    ffErrorHandler::raise("Separator present in values", E_USER_ERROR, $this, get_defined_vars());
                                }
                            }
                            reset($Field->recordset);
                        }
                        break;
                        
                    default:
                        ffErrorHandler::raise("Invalid Grouping Action with base_type 'Text'", E_USER_ERROR, $this, get_defined_vars());
                }
                break;
                
            default:
                ffErrorHandler::raise("Invalid Grouping with base_type different from 'Text'", E_USER_ERROR, $this, get_defined_vars());
        }
        
        // THE REAL STUFF
        if ($Field->parent !== null && strlen($Field->parent[0]->id)) {
            $tpl_id = $Field->parent[0]->id;
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
            
            $field_container = $Field->parent[0]->id . "_";
            $this->tpl[$tpl_id]->set_var("container", $field_container);
            $prefix = $Field->parent[0]->id . "_";
        } else {
            $tpl_id = "main";
            if (!isset($this->tpl[$tpl_id])) {
                $this->prepare_template($tpl_id);
            }
        }
            
        $this->tpl[$tpl_id]->set_var("id", $id);
        $this->tpl[$tpl_id]->set_var("site_path", $Field->parent_page[0]->site_path);
        $this->tpl[$tpl_id]->set_var("theme", $Field->getTheme());

        if (strlen($Field->widget_path)) {
            $this->tpl[$tpl_id]->set_var("widget_path", $Field->widget_path);
        } else {
            $this->tpl[$tpl_id]->set_var("widget_path", "/themes/responsive/ff/ffField/widgets/checkgroup");
        }

        $this->tpl[$tpl_id]->set_var("separator", $Field->grouping_separator);

        $selected_values = explode($Field->grouping_separator, $value->getValue());

        if (!is_array($Field->properties)) {
            $Field->properties = array();
        }

        $Field->properties["onchange"] = " ff.ffField.checkgroup.recalc('" . $field_container . $id . "', this); " . $Field->properties["onchange"];
        
        if (count($Field->recordset)) {
            $this->tpl[$tpl_id]->set_var("SectRow", "");
            $i = 0;
            $data_filled = false;
            foreach ($Field->recordset as $tmp_key => $tmp_value) {
                $this->tpl[$tpl_id]->set_var("index", $i);
                $this->tpl[$tpl_id]->set_var("element_value", $tmp_value[0]->getValue());
                $this->tpl[$tpl_id]->set_var("label", ffCommon_specialchars($tmp_value[1]->getValue($Field->multi_app_type, FF_LOCALE)));
                
                $class = $this->class;
                $control_class = $Field->get_control_class("checkbox");
                if (in_array($tmp_value[0]->getValue(), $selected_values)) {
                    $this->tpl[$tpl_id]->set_var("checked", "checked=\"checked\"");
                    $class = $class . ($class ? " " : "") . "on";
                    $data_filled = true;
                } else {
                    $this->tpl[$tpl_id]->set_var("checked", "");
                    $class = $class . ($class ? " " : "") . "off";
                }
                
                $class .= " " . cm_getClassByFrameworkCss("", "row-default");
                $class .= " checkbox";

                $this->tpl[$tpl_id]->set_var("class", $class);
                $this->tpl[$tpl_id]->set_var("control_class", $control_class);
                $this->tpl[$tpl_id]->set_var("properties", $Field->getProperties());
                
                $this->tpl[$tpl_id]->parse("SectRow", true);
                $i++;
            }
            reset($Field->recordset);

            if ($data_filled) {
                $this->tpl[$tpl_id]->set_var("value", ffCommon_specialchars($value->getValue()));
            } else {
                $this->tpl[$tpl_id]->set_var("value", "");
            }
            $this->tpl[$tpl_id]->set_var("length", $i);
            
            $this->tpl[$tpl_id]->parse("SectBinding", true);
            return $this->tpl[$tpl_id]->rpparse("SectControl", false);
        } else {
            return "No data to select";
        }
    }

    public function get_component_headers($id)
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddJs("ff.ffField", "ffField.js", FF_THEME_DIR . "/library/ff");
            $this->oPage[0]->tplAddJs("ff.ffField.checkgroup", "checkgroup.js", FF_THEME_DIR . "/responsive/ff/ffField/widgets/checkgroup");
        }
        
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectHeaders", false);
    }
    
    public function get_component_footers($id)
    {
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectFooters", false);
    }
    
    public function process_headers()
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddJs("ff.ffField", "ffField.js", FF_THEME_DIR . "/library/ff");
            $this->oPage[0]->tplAddJs("ff.ffField.checkgroup", "checkgroup.js", FF_THEME_DIR . "/responsive/ff/ffField/widgets/checkgroup");
            
            //return;
        }

        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectHeaders", false);
    }
    
    public function process_footers()
    {
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectFooters", false);
    }
}
