<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"cookiebar" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.cookiebar",
							"file" => "jquery.cookiebar.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.cookiebar",
									"file" => "jquery.cookiebar.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
