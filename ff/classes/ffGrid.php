<?php
/**
 * Data Grid
 *
 * @package FormsFramework
 * @subpackage components
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */

/**
 * Data Grid
 *
 * @package FormsFramework
 * @subpackage components
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */
class ffGrid
{
    protected static $events = null;
    
    public function __construct()
    {
        ffErrorHandler::raise("Cannot istantiate " . __CLASS__ . " directly, use ::factory instead", E_USER_ERROR, $this, get_defined_vars());
    }
    
    public function __clone()
    {
        ffErrorHandler::raise("Cannot clone " . __CLASS__ . ", use ::factory instead", E_USER_ERROR, $this, get_defined_vars());
    }

    public static function addEvent($event_name, $func_name, $priority = null, $index = 0, $break_when = null, $break_value = null, $additional_data = null)
    {
        self::initEvents();
        self::$events->addEvent($event_name, $func_name, $priority, $index, $break_when, $break_value, $additional_data);
    }

    public static function doEvent($event_name, $event_params = array())
    {
        self::initEvents();
        return self::$events->doEvent($event_name, $event_params);
    }
    
    private static function initEvents()
    {
        if (self::$events === null) {
            self::$events = new ffEvents();
        }
    }

    /**
     * Questa funzione crea un'istanza di ffGrid utilizzando i parametri in ingresso
     *
     * @param ffPage_base $page
     * @param string $disk_path
     * @param string $theme
     * @param array $variant
     * @return ffGrid_base
     */
    public static function factory(ffPage_base $page, $disk_path = null, $theme = null, array $variant = null)
    {
        if ($theme === null) {
            $theme = $page->theme;
        }
            
        if ($disk_path === null) {
            $disk_path = $page->disk_path;
        }
            
        $res = self::doEvent("on_factory", array($page, $disk_path, $theme, $variant));
        $last_res = end($res);

        if (is_null($last_res)) {
            $base_path = $disk_path . "/themes/" . $theme;
            
            if (!isset($variant["name"])) {
                $registry = ffGlobals::getInstance("_registry_");
                if (!isset($registry->themes) || !isset($registry->themes[$theme])) {
                    $registry->themes[$theme] = new SimpleXMLElement($base_path . "/theme_settings.xml", null, true);
                }
        
                $suffix = $registry->themes[$theme]->default_class_suffix;
                
                $class_name = __CLASS__ . "_" . $suffix;
            } else {
                $class_name = $variant["name"];
            }
                
            if (!isset($variant["path"])) {
                $base_path .= "/ff/" . __CLASS__ . "/" . $class_name . "." . FF_PHP_EXT;
            } else {
                $base_path .= $variant["path"];
            }
        } else {
            $base_path = $last_res["base_path"];
            $class_name = $last_res["class_name"];
        }
        
        require_once $base_path;
        $tmp = new $class_name($page, $disk_path, $theme);
        
        $res = self::doEvent("on_factory_done", array($tmp));

        return $tmp;
    }
}

/**
 * ffGrid è la classe che gestisce la visualizzazione di liste di dati.
 * Per quanto permetta di includere pulsanti d'azione e veri e propri controlli,
 * di proprio non esegue operazioni di nessun tipo sui dati.
 *
 * @package FormsFramework
 * @subpackage components
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */
abstract class ffGrid_base extends ffCommon
{
    // ----------------------------------
    //  PUBLIC VARS (used for settings)

    /*
        var $framework_css = array(
                "component" => array(
                    "inner_wrap" => false // null OR false OR true OR array(xs, sm, md, lg) OR 'row-default' OR 'row' OR 'row-fluid'
                    , "outer_wrap" => false //false OR true OR array(xs, sm, md, lg) OR 'row-default' OR 'row' OR 'row-fluid'
                    , "grid" => false        //false OR array(xs, sm, md, lg) OR 'row' OR 'row-fluid'
                )
                , "title" => array(
                        "class" => null
                        , "col" => array(
                                    "xs" => 12
                                    , "sm" => 12
                                    , "md" => 5
                                    , "lg" => 5
                                )
                        )
                , "actionsTop" => array(
                        "class" => null
                        , "col" => array(
                                "xs" => 12
                                , "sm" => 12
                                , "md" => 7
                                , "lg" => 7
                        )
                        , "util" => array(
                            "right"
                        )
                )
                , "description" => array(
                        "class" => null
                        , "callout" => "info"
                        , "col" => array(
                                "xs" => 12
                                , "sm" => 12
                                , "md" => 12
                                , "lg" => 12
                        )
                )
                , "search" => array(
                    "class" => null
                    , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 5
                            , "lg" => 5
                    )
                )
                , "navigatorTop" => array(
                    "class" => null
                    , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 12
                            , "lg" => 12
                    )
                )
                , "table" => array(
                    "class" => "ffGrid"
                    , "col" => false
                    , "util" => "table"
    
                )
                , "filters" => array(
                    "class" => null
                    , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 12
                            , "lg" => 12
                    )
                )
                , "navigatorBottom" => array(
                    "class" => null
                    , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 12
                            , "lg" => 12
                    )
                    , "util" => array(
                        "right"
                    )
                )
                , "alphanum" => array(
                    "class" => null
                    , "col" => array(
                        "xs" => 8
                        , "sm" => 8
                        , "md" => 6
                        , "lg" => 5
                    )
                )
                , "actionsBottom" => array(
                    "class" => null
                    , "col" => array(
                        "xs" => 12
                        , "sm" => 12
                        , "md" => 12
                        , "lg" => 12
                    )
                    , "util" => array(
                        "right"
                    )
                )
                , "field" => array(
                    "label" => array(
                        "class" => null
                        , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 6
                            , "lg" => 6
                        )
                    )
                    , "control" => array(
                        "class" => null
                        , "col" => array(
                            "xs" => 12
                            , "sm" => 12
                            , "md" => 6
                            , "lg" => 6
                        )
                    )
                )
        );*/

    /**
     * ID dell'oggetto; deve essere univoco per ogni ffPage
     * @var Number
     */
    public $id						= "";

    /**
     * URL relativo al web del sito
     * @var String
     */
    public $site_path 				= "";
    
    /**
     * URL relativo al disco del sito
     * @var String
     */
    public $disk_path 				= "";

    /**
     * Cartella dove è contenuta la pagina partendo dalla root del sito
     * @var String
     */
    public $page_path 				= "";
    public $class	 				= "ffGrid";					//

    /**
     * Cartella del template; di default è la cartella "theme"
     * @var String
     */
    public $template_dir			= null;

    public $max_rows				= null;

    /**
     * File del template; di default è il file "ffGrid.html"
     * @var String
     */
    public $template_file 			= "ffGrid.html";
    
    public $fixed_pre_content 		= "";
    public $fixed_post_content 	= "";
    
    public $fixed_title_content 	= "";
    public $fixed_heading_content 	= "";
    public $fixed_body_content 	= "";
    
    public $theme					= null;

    /**
     * Default settato a null; equivale a FF_SITE_PATH
     * @var String
     */
    public $redirect_path			= null;

    public $display				= true;					// force ffPage to hide the whole component

    public $redirect_fullscreen	= false;
    public $redirect_theme			= null;
    public $redirect_layer			= null;
    public $redirect_topbar		= null;
    public $redirect_navbar		= null;

    public $dialog_path			= null;					/* default to null, means FF_SITE_PATH
                                                            NB:
                                                            Forms add "/dialog" to this value, so if you copying redirect
                                                            files you are forced to enclose in a so-called directory. */
    public $properties 			= array(
                                              "Frame" 			=> array(
                                                         "class" 				=> "ffGrid"
                                                       , "width"				=> null
                                                                )
                                            , "Grid_Frame"	 	=> array(
                                                         "class" 				=> "DataGrid"
                                                                )
                                            , "Grid_Table"	 	=> array(
                                                         "class" 				=> "frame"
                                                       , "width"				=> null
                                                                )
                                        );

    public $widget_deps			= null;

    public $processed_widgets = array();
    
    /**
     * Link alla pagina precedente;
     * @var String
     */
    public $ret_url				= "";

    public $use_fixed_fields		= false;				/* If enabled, force to use normal vars & sections on fixed
                                                            fields Templates instead on generating them.
                                                            Sections will have same fields names with "SectSet" prefix for
                                                            fields with values, "SectNotSet" for empty fields. */

    public $fixed_vars = array();

    public $force_no_field_params		= false;
    public $include_all_records		= false;
    public $use_own_location		= false;
    public $location_name			= null;

    /**
     * Titolo della grid
     * @var String
     */
    public $title					= "";

    /**
     * Select SQL per la Grid; ricorsarsi di mettere aggiungere i tag [WHERE] e [ORDER]
     * Se vengono aggiunte delle condizioni di WHERE e ORDER proprie bisogna inoltre aggiungere anche i tag
     * [AND] o [OR] nella clausola WHERE e il tag [COLON] nella clausola ORDER.
     * Esempi:
     * "SELECT * FROM users	 WHERE status = '1' [AND] [WHERE] ORDER BY level [COLON] [ORDER]"
     * "SELECT * FROM users [WHERE] ORDER BY level [COLON] [ORDER]"
     * "SELECT * FROM users	[WHERE]	[ORDER]	LIMIT 1000"
     *
     * @var String
     */
    public $source_SQL 			= "";
    public $source_DS				= null;
    
    public $sql_check = true;

    /**
     * Abilita l'ordinamento all'interno della Grid
     * @var Boolean
     */
    public $use_order 				= true;

    /**
     * Abilita la ricerca all'interno della grid
     * @var String
     */
    public $use_search				= true;

    /**
     * Visualizza il BOX per la ricerca
     * @var Boolean
     */
    public $display_search		 	= true;
    
    
    public $search_container		= null;
    public $search_container_buffer = "";
    
    public $search_method			= "get"; 				/* use 'post' only if you have a textarea in search box
                                                            or if you have a lot of fields to search on */
    public $search_cols			= 2;					// how many field/label pairs per rows
    public $search_url				= "";					// leave blank for script page

    /**
     * le opzioni dei pulsanti di default
     * @var Mixed
     */
    public $buttons_options		= array(
                                    "search" => array(
                                              "display" => true
                                            , "index" 	=> 0
                                            , "obj" 	=> null
                                            , "label" 	=> null
                                            , "class"	=> null
                                            , "icon"	=> null
                                            , "aspect"	=> "link"
                                    )
                                    , "delete" => array(
                                            "class"		=> null
                                            , "label"	=> null
                                            , "aspect"    => "link"
                                            , "disposition" => array("row" => 1, "col" => null)
                                    )
                                    , "edit" => array(
                                            "class"		=> null
                                            , "label"	=> null
                                            , "icon"    => null
                                            , "aspect"  => "link"
                                            , "disposition" => array("row" => 1, "col" => null)
                                    )
                                    , "addnew" => array(
                                            "class"		=> null
                                            , "label"	=> null
                                            , "icon"	=> null
                                            , "aspect"	=> "link"
                                    )
                                    , "export" => array(
                                            "class"		=> null
                                            , "display" => false
                                            , "index" 	=> 0
                                            , "obj" 	=> null
                                            , "label" 	=> null
                                            , "icon"	=> null
                                            , "aspect"	=> "link"
                                    )
                                );


    /**
     * Visualizza il pulsante "Aggiungi"
     * @var Boolean
     */
    public $display_new			= true;
    public $display_not_add_new	= true;

    public $display_actions		= true;

    public $display_grid			= "always";				// always: display even without search | search: display only when a search is done | never: mah.. :)

    /**
     * ID del recordo associato alla grid
     * @var String
     */
    public $record_id				= "";					// the record object name

    /**
     * url del record associato alla grid
     * @var String
     */
    public $record_url				= "";
    public $record_url_delete		= null;

    /**
     * Link al record per l'inserimento dei dati (pagina di tipo ffRecord).
     * Se non impostato verrà utilizzato $record_url
     * @var String
     */
    public $record_insert_url		= "";

    public $record_delete_url		= "";
    
    public $bt_edit_url 			= "";
    public $bt_delete_url 			= "";
    public $bt_insert_url 			= "";
    
    public $addit_record_param     = "";
    public $addit_insert_record_param     = "";

    /**
     * Determinerà se i record verranno suddivisi su più pagine.
     * @var Boolean
     */
    public $use_paging			= true;

    /**
     * Numero di record per ogni pagina
     * @var Number
     */
    public $default_records_per_page	= 25;
    public $default_page	= 1;
    public $default_nav_selector_elements = array(10, 25, 50);

    /**
     * Visualizza il page navigator
     * @var boolean
     */
    public $display_navigator		= true;
    public $navigator				= null;					/* a ffPageNavigator object. Read ffPageNavigator
                                                            docs for personalization. If you don't instantiate it
                                                            explicity, ffGrid do it for you with standard
                                                            vals */
    public $navigator_orientation	= "both";				// may be: both, top or bottom. Use you immagination..

    public $display_labels			= true;					/* display column's labels. If you don't display them,
                                                            you will not able to interactively order */
    public $order_method			= "both";				/* 	none: 	normal useless poor labels =D
                                                            labels: order by clicking on labels
                                                            icons:	order by clicking on a little pair of icon
                                                            both:	use both methods (more cool! :-) */

    /**
     * Campo che viene utilizzato per l'ordinamento;
     * la variabile va OBBLIGATORIAMENTE impostata.
     * @var String
     */
    public $order_default			= "";

    public $use_alpha				= false;				/* determine if records will be filtered using alphabetical
                                                            index. */

    public $alpha_field			= null;					/* the id of the field to filter by.
                                                            required if alpha indexing is used */

    public $alpha_default			= "";					/* the letter to filter by default. setting this to empty string
                                                            cause to display all records. */


    // LABELS

    /**
     * Label che viene visualizzata per la conferma dell'eliminazione del record
     * @var String
     */
    public $label_delete			= "Confermi l&apos;eliminazione del dato?<br /><span>Il dato verr&agrave; eliminato definitivamente, non potr&agrave; essere recuperato.</span>";

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $parent					= null;					// The containing class
    public $prefix					= "";					// Global temp var for params prefix.

    /**
     * Campi nascosti associati a ffGrid
     * @var array()
     */
    public $hidden_fields			= array();				// array of hidden ffFields

    /**
     * Array che contiene i campi di ricerca della grid
     * @var array()
     */
    public $search_fields			= array();				// array of ffFields to search for

    /**
     * Array che contiene i campi della grid
     * @var array()
     */
    public $grid_fields			= array();				// array of ffFields to display in grid
    
    public $grid_disposition	= null;				// array of ffFields and fButtons to display in grid  by Row and Col
    public $grid_disposition_options = array(		// Wrap elem with same type in the same Col
        "button" => array(
            "wrap_col" => false
        )
        , "field" => array(
            "wrap_col" => false
        )
    );


    /**
     * Array contenente i campi chiave della grid
     * @var array()
     */
    public $key_fields				= array();				/* array of key fields. to use in conjuction with
                                                            edit or/and delete buttons */

    public $recordset_keys			= array();
    public $recordset_values		= array();
    public $displayed_keys			= array();
    
    public $recordset_ori_values 	= array();

    /**
     * Array contenente i pulsanti associati ad ogni recordo della grid
     * @var array()
     */
    public $grid_buttons			= array();
    
    /**
     * Array di pulsanti associati alla grid.
     * @var array()
     */
    public $action_buttons			= array();
    public $action_buttons_header  = array();
    /**
     * Array di pulsanti visualizzati sotto il box della ricerca
     * @var array()
     */
    public $search_buttons			= array();

    public $fields_values			= array();

    public $navigator_params		= "";					// navigation vars in url form
    public $order_params			= "";					// orders vars in url form
    public $search_params			= "";					// search vars in url form
    public $hidden_params			= "";					// hidden vars in url form

    public $params					= "";					// a collection of all of the above

    public $page					= "";					/* current page displayed (view page_navigator in main Form file)
                                                           automatically set to 1 by the code if not set */
    public $frame					= "";					// as above
    public $records_per_page		= "";					// as above
    public $nav_selector_elements  = "";

    /**
     * Azione eseguita sul form
     * @var String
     */
    public $frmAction				= "";

    /**
     * Descrizione dell'errore
     * @var String
     */
    public $strError				= "";

    public $alpha					= "";					// the actual letter to filter by

    public $searched				= false;					// hold if a search is started

    public $order				    = "";					// the actual field
    public $direction				= "";					// the actual direction

    /**
     * Ricerca nella sintassi SQL
     * @var String
     */
    public $sqlWhere				= "";

    /**
     * Ordinamento nella sintassi SQL
     * @var String
     */
    public $sqlOrder				= "";

    /**
     * HAVING nella sintassi SQL
     * @var String
     */
    public $sqlHaving				= "";

    /**
     * SQL processata da ffGrid
     * @var String
     */
    public $processed_SQL			= null;

    /**
     * L'uRL completo della pagina
     * @var String
     */
    public $url					= "";
    public $full_record_url		= "";					// full parameterized record url without keys

    public $use_fields_params		= false;

    public $db						= null;					// Internal DB_Sql() Object
    public $tpl					= null;					// Internal ffTemplate() object
    
    private $parsed_fields			= 0;
    private $parsed_filters			= 0;
    private $parsed_hidden_fields 	= 0;

    public $cache_get_resources	= array();
    public $cache_clear_resources	= array();

    public $json_result = array();

    //var $additional_vars		= array();

    public $resources = array();
    public $resources_set = array();
    public $resources_get = array();
    public $grid_disposition_elem = array();

    // ---------------------------------------------------------------
    //  ABSTRACT FUNCS (depends on theme)
    // ---------------------------------------------------------------

    abstract protected function tplLoad();
    abstract public function tplParse($output_result);
    abstract public function structProcess($tpl);
    
    // ---------------------------------------------------------------
    //  PUBLIC FUNCS
    // ---------------------------------------------------------------

    /**
     * constructor
     * @param ffPage_base $page l'istanza di page associata
     * @param String $disk_path il percorso su disco del tema
     * @param String $theme il tema impiegato
     */
    public function __construct(ffPage_base $page, $disk_path, $theme)
    {
        $this->get_defaults("ffGrid");
        $this->get_defaults();

        $this->site_path = $page->site_path;
        $this->page_path = $page->page_path;
        $this->disk_path = $disk_path;
        $this->theme = $theme;

        if ($this->db === null) {
            $this->db[0] = ffDb_Sql::factory();
        }

        if ($this->use_paging && $this->navigator === null) {
            $this->navigator[0] = ffPageNavigator::factory($page, $this->disk_path, $this->site_path, $this->page_path, $this->theme);
        }
    }

    // -----------------------
    //  Internal Array funcs

    /**
     * Aggiunge un campo hidden a ffGrid
     * @param ffField Campo da aggiungere
     * @param String $value
     */
    public function addHiddenField($field, $value = null)
    {
        if ($value !== null && !get_class($value) == "ffData") {
            ffErrorHandler::raise("Wrong call to addHiddenField: value must be a ffData", E_USER_ERROR, $this, get_defined_vars());
        }
    
        $this->hidden_fields[$field] = $value;
    }

    /**
     * Aggiunge un campo di ricerca a ffGrid
     * @param ffField Il campo di ricerca
     */
    public function addSearchField(ffField_base $field)
    {
        if (!is_subclass_of($field, "ffField_base")) {
            ffErrorHandler::raise("Wrong call to addSearchField: object must be a ffField", E_USER_ERROR, $this, get_defined_vars());
        }
    
        $field->cont_array =& $this->search_fields;
        $field->parent = array(&$this);
        $this->search_fields[$field->id] = $field;
    }

    /**
     * Aggiunge un campo che verrà visualizzato
     * @param ffField Il campo da aggiungere
     * @param boolean
     */
    public function addContent($field, $toOrder = true)
    {
        if (!is_subclass_of($field, "ffField_base")) {
            ffErrorHandler::raise("Wrong call to addContent: object must be a ffField", E_USER_ERROR, $this, get_defined_vars());
        }

        $field->parent = array(&$this);
        $field->cont_array =& $this->grid_fields;
        
        $this->grid_fields[$field->id] = $field;
        if ($toOrder && $field->data_type !== "" && $this->use_order) {
            $this->grid_fields[$field->id]->allow_order = true;
        }
    }

    /**
     * Aggiunge un campo chiave a ffGrid
     * @param ffField Il campo da aggiungere
     * @param boolean
     */
    public function addKeyField($field, $bGlobal = false)
    {
        if (!is_subclass_of($field, "ffField_base")) {
            ffErrorHandler::raise("Wrong call to addKeyField: object must be a ffField", E_USER_ERROR, $this, get_defined_vars());
        }

        $field->parent = array(&$this);
        $field->cont_array =& $this->key_fields;
        $this->key_fields[$field->id] = $field;
        $this->key_fields[$field->id]->is_global = $bGlobal;
    }

    /**
     * Aggiunge un pulsante a ffGrid; il pulsante viene aggiunto ad ogni riga di ffGrid
     * @param ffButton $button
     */
    public function addGridButton($button, $col = null, $row = null, $params = null)
    {
        if (!is_subclass_of($button, "ffButton_base")) {
            ffErrorHandler::raise("Wrong call to addGridButton: object must be a ffButton", E_USER_ERROR, $this, get_defined_vars());
        }

        $button->parent = array(&$this);
        $this->grid_buttons[$button->id] = $button;
        
        $this->addGridDisposition($button->id, "button", $col, $row, $params);
    }

    /**
     * Aggiunge un pulsante a ffGrid; in questo caso il pulsante viene aggiungo dopo ffGrid e non ad ogni riga.
     * @param ffButton $button
     */
    public function addActionButton($button)
    {
        if (!is_subclass_of($button, "ffButton_base")) {
            ffErrorHandler::raise("Wrong call to addActionButton: object must be a ffButton", E_USER_ERROR, $this, get_defined_vars());
        }

        $button->parent = array(&$this);
        $this->action_buttons[$button->id] = $button;
    }

    /**
     * Aggiunge un pulsante a ffGrid; in questo caso il pulsante viene aggiunto accanto al bottone aggiungi nuovo e non ad ogni riga.
     * @param ffButton $button
     */
    public function addActionButtonHeader($button)
    {
        if (!is_subclass_of($button, "ffButton_base")) {
            ffErrorHandler::raise("Wrong call to addActionButton: object must be a ffButton", E_USER_ERROR, $this, get_defined_vars());
        }

        $button->parent = array(&$this);
        $this->action_buttons_header[$button->id] = $button;
    }
    
    public function addGridDisposition($ID_component, $component_type, $col = null, $row = null, $params = null)
    {
        $default_type = array(
            "button" => -1
        );
        
        if (!count($this->grid_disposition)) {
            $this->grid_disposition[] = array();
        }

        if ($row === null) {
            $row = 0;
        } elseif (is_numeric($row)) {
            $row = $row - 1;
        } elseif ($row == "last") {
            $row = count($this->grid_disposition) - 1;
        }
        
        if ($row < 0) {
            $row = 0;
        }
                
        if ($row - count($this->grid_disposition) > 1) {
            $row = count($this->grid_disposition) + 1;
        }

        if ($col === null) {
            $col = count($this->grid_disposition[$row]);

            if ($this->grid_disposition_options[$component_type]["wrap_col"]  && isset($default_type[$component_type]) && $col > 0 && count($this->grid_disposition_elem["data"][$row][$col + $default_type[$component_type]]) == 1 && isset($this->grid_disposition_elem["data"][$row][$col + $default_type[$component_type]][$component_type])) {
                $col = $col + $default_type[$component_type];
            }
        } elseif (is_numeric($col)) {
            $col = $col - 1;
        } elseif ($col == "last") {
            $col = count($this->grid_disposition[$row]) - 1;
        }
        
        if ($col < 0) {
            $col = count($this->grid_disposition[$row]);
        }
            
        if ($col - count($this->grid_disposition[$row]) > 1) {
            $col = count($this->grid_disposition[$row]) + 1;
        }

        $params["type"] = $component_type;

        $this->grid_disposition[$row][$col][$ID_component] = $params;
        
        $this->grid_disposition_elem["data"][$row][$col][$component_type]++;
        $this->grid_disposition_elem["count"][$row]++;
    }
    /**
     * Aggiunge un pulsante di ricerca
     * @param ffButton_base $button
     * @param int $index l'ordine in cui comparirà il pulsante
     */
    public function addSearchButton($button, $index = null)
    {
        if (!is_subclass_of($button, "ffButton_base")) {
            ffErrorHandler::raise("Wrong call to addSearchButton: object must be a ffButton", E_USER_ERROR, $this, get_defined_vars());
        }

        $button->parent = array(&$this);
        if ($index === null) {
            $this->search_buttons[] = $button;
        } else {
            array_splice(
                $this->search_buttons,
                $index,
                0,
                array(&$button)
                        );
        }
    }

    public function getSQL()
    {
        if ($this->source_DS !== null) {
            if (is_string($this->source_DS)) {
                return ffDBSource::getSource($this->source_DS)->getSql($this);
            } else {
                return $this->source_DS->getSql($this);
            }
        } else {
            return $this->source_SQL;
        }
    }
    
    /**
     * process preparation' function
     * usually called by ffPage
     */
    public function pre_process()
    {
        // Load Template and initialize it
        $this->tplLoad();
            
        // First of all, process all page's params
        $this->process_params();
        
        // Finally, prepare SQL
        $sSQL = $this->getSQL();

        if (strlen($sSQL)) {
            $sSQL = $this->source_SQL;

            $bFindWhereTag = preg_match("/\[WHERE\]/", $sSQL);
            $bFindWhereOptions = preg_match("/(\[AND\]|\[OR\])/", $sSQL);
            $bFindOrderTag = preg_match("/\[ORDER\]/", $sSQL);
            $bFindOrderOptions = preg_match("/\[COLON\]/", $sSQL);
            $bFindHavingTag = preg_match("/\[HAVING\]/", $sSQL);
            $bFindHavingOptions = preg_match("/(\[HAVING_AND\]|\[HAVING_OR\])/", $sSQL);

            // Do some (pathetic) SQL syntax check
            if ($this->sql_check) {
                if (!$bFindWhereTag) {
                    ffErrorHandler::raise("SQL Statement without '[WHERE]' Tag in the SQL!", E_USER_ERROR, $this, get_defined_vars());
                } elseif (!$bFindOrderTag) {
                    ffErrorHandler::raise("SQL Statement without '[ORDER]' Tag in the SQL", E_USER_ERROR, $this, get_defined_vars());
                } elseif (!$bFindHavingTag && strlen($this->sqlHaving)) {
                    ffErrorHandler::raise("SQL statement with 'having' fields and without a '[HAVING]' tag", E_USER_ERROR, $this, get_defined_vars());
                }
            }

            if (strlen($this->sqlWhere)) {
                if ($bFindWhereOptions) {
                    $tmp = "";
                } else {
                    $tmp = " WHERE ";
                }
                $sSQL = str_replace("[WHERE]", $tmp . $this->sqlWhere, $sSQL);
                $sSQL = str_replace("[AND]", "AND", $sSQL);
                $sSQL = str_replace("[OR]", "OR", $sSQL);
            } else {
                // remove tags, if exist
                $sSQL = str_replace("[WHERE]", "", $sSQL);
                $sSQL = str_replace("[AND]", "", $sSQL);
                $sSQL = str_replace("[OR]", "", $sSQL);
            }

            if (strlen($this->sqlHaving)) {
                if ($bFindHavingOptions) {
                    $tmp = "";
                } else {
                    $tmp = " HAVING ";
                }
                $sSQL = str_replace("[HAVING]", $tmp . "(" . $this->sqlHaving . ")", $sSQL);
                $sSQL = str_replace("[HAVING_AND]", "AND", $sSQL);
                $sSQL = str_replace("[HAVING_OR]", "OR", $sSQL);
            } else {
                // remove tags, if exist
                $sSQL = str_replace("[HAVING]", "", $sSQL);
                $sSQL = str_replace("[HAVING_AND]", "", $sSQL);
                $sSQL = str_replace("[HAVING_OR]", "", $sSQL);
            }

            if (strlen($this->sqlOrder)) {
                if ($bFindOrderOptions) {
                    $tmp = "";
                } else {
                    $tmp = " ORDER BY ";
                }
                $sSQL = str_replace("[ORDER]", $tmp . $this->sqlOrder, $sSQL);
                $sSQL = str_replace("[COLON]", ",", $sSQL);
            } else {
                // remove tags, if exist
                $sSQL = str_replace("[ORDER]", "", $sSQL);
                $sSQL = str_replace("[COLON]", "", $sSQL);
            }
            //---------------------------------------------

            $this->processed_SQL = $sSQL;
        }
    }

    /**
     * process function
     * usually called by ffPage
     */
    public function process()
    {
        // manage actions. This may cause a redirect, so the end is never reached
        $this->process_action();
    }

    /**
     * interface' processing function
     * @param boolean $output_result
     * @return mixed
     */
    public function process_interface($output_result = false)
    {
        $res = ffGrid::doEvent("on_before_process_interface", array(&$this));
        $rc = end($res);
        if ($rc !== null) {
            return;
        }

        $res = $this->doEvent("on_before_process_interface", array(&$this));
        $rc = end($res);
        if ($rc !== null) {
            return;
        }

        // display error
        if ($this->tpl !== null) {
            $this->displayError();

            // process all section
            $this->initControls();

            $this->process_action_buttons();
            $this->process_action_buttons_header();
            
            $this->process_hidden();
        }
        $this->process_grid();		// process navigator and labels (with order) from inside

        if ($output_result !== null) {
            return $this->tplParse($output_result);
        }
    }
    
    public function getProperties($property_set)
    {
        $buffer = "";
        if (is_array($property_set) && count($property_set)) {
            foreach ($property_set as $key => $value) {
                if ($key == "style") {
                    if (strlen($buffer)) {
                        $buffer .= " ";
                    }
                    $buffer .= $key . "=\"";
                    foreach ($property_set[$key] as $subkey => $subvalue) {
                        $buffer .= $subkey . ": " . $subvalue . ";";
                    }
                    reset($property_set[$key]);
                    $buffer .= "\"";
                } elseif (strlen($value)) {
                    if (strlen($buffer)) {
                        $buffer .= " ";
                    }
                    $buffer .= $key . "=\"" . $value . "\"";
                }
            }
            reset($property_set);
        }
        return $buffer;
    }
    
    /**
     * Restituisce il tema utilizzato da ffGrid
     * @return String Tema utilizzato da ffGrid
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Restituisce il percorso del template utilizzato da ffGrid
     * @return String path del template utilizzato da ffGrid
     */
    public function getTemplateDir()
    {
        $res = $this->doEvent("getTemplateDir", array($this));
        $last_res = end($res);
        if ($last_res === null) {
            if ($this->template_dir === null) {
                return $this->disk_path . "/themes/" . $this->getTheme() . "/ff/ffGrid";
            } else {
                return $this->template_dir;
            }
        } else {
            return $last_res;
        }
    }
    
    /**
     * params processing function
     * usually called by ffPage
     */
    public function process_params()
    {
        $this->process_alpha_params();
        $this->process_search_params();
        $this->process_order_params();
        $this->process_navigator_params();
        if (!$this->force_no_field_params) {
            $this->process_fields_params();
        }
        $this->process_hidden_params();

        $this->params = $this->order_params;
        if ($this->use_search) {
            $this->params .= "&" . $this->search_params;
        }
        if ($this->use_paging) {
            $this->params .= "&" . $this->navigator_params;
        }
        $this->params .= $this->hidden_params;
    }

    /**
     * field's params processing
     * called by process_params()
     */
    public function process_fields_params()
    {
        $this->frmAction = $this->parent[0]->retrieve_param($this->id, "frmAction");

        if (!$this->force_no_field_params) {
            foreach ($this->grid_fields as $key => $value) {
                if (strlen($this->grid_fields[$key]->control_type)) {
                    $this->use_fields_params = true;
                }
            }
            reset($this->grid_fields);
        }

        // retrieve global recordset (handle all values)
        $this->recordset_keys = $this->parent[0]->retrieve_param($this->id, "recordset_keys");
        $this->recordset_values = $this->parent[0]->retrieve_param($this->id, "recordset_values");
        $this->recordset_ori_values = $this->parent[0]->retrieve_param($this->id, "recordset_ori_values");

        if (!is_array($this->recordset_keys)) {
            $this->recordset_keys = array();
        }

        if (!is_array($this->recordset_values)) {
            $this->recordset_values = array();
        }

        if (!is_array($this->recordset_ori_values)) {
            $this->recordset_ori_values = array();
        }

        //				ffErrorHandler::raise("DEBUG IN CORSO", E_USER_ERROR, $this, get_defined_vars());

        ksort($this->recordset_keys, SORT_NUMERIC);
        ksort($this->recordset_values, SORT_NUMERIC);
        ksort($this->recordset_ori_values, SORT_NUMERIC);

        // retrieve last displayed recordset and exclude from global recordset if needed
        // use of displayed recordset is required to use proper locale/app_type
        $this->displayed_keys = $this->parent[0]->retrieve_param($this->id, "displayed_keys");

        if (is_array($this->displayed_keys) && count($this->displayed_keys)) {
            ksort($this->displayed_keys, SORT_NUMERIC);
            foreach ($this->displayed_keys as $rst_key => $rst_value) {
                // determine if record is changed and store values
                $include_record = false;
                foreach ($this->grid_fields as $key => $FormField) {
                    if ($this->grid_fields[$key]->control_type != "") {
                        if (isset($this->recordset_values[$rst_key][$key])) {
                            $tmp = new ffData($this->recordset_values[$rst_key][$key], $this->grid_fields[$key]->get_app_type(), FF_LOCALE);
                        } else {
                            $tmp = $this->grid_fields[$key]->getDefault(array(&$this));
                        }
                        $tmp2 = new ffData($this->recordset_ori_values[$rst_key][$key], $this->grid_fields[$key]->get_app_type(), FF_LOCALE);

                        if ($tmp->getValue($this->grid_fields[$key]->base_type, FF_SYSTEM_LOCALE) !== $tmp2->getValue($this->grid_fields[$key]->base_type, FF_SYSTEM_LOCALE)) {
                            $include_record = true;
                            $this->recordset_values[$rst_key][$key] = $tmp->getValue($this->grid_fields[$key]->base_type, FF_SYSTEM_LOCALE);
                        }
                    }
                }
                reset($this->grid_fields);

                if (!$include_record && !$this->include_all_records) {
                    unset($this->recordset_keys[$rst_key]);
                    unset($this->recordset_values[$rst_key]);
                    unset($this->recordset_ori_values[$rst_key]);
                }
            }
            reset($this->displayed_keys);

            ksort($this->recordset_keys, SORT_NUMERIC);
            ksort($this->recordset_values, SORT_NUMERIC);
            ksort($this->recordset_ori_values, SORT_NUMERIC);

            // recalc ids (array_merge weird behavoir)
            $this->recordset_keys = array_merge($this->recordset_keys, array());
            $this->recordset_values = array_merge($this->recordset_values, array());
            $this->recordset_ori_values = array_merge($this->recordset_ori_values, array());
        }

        // reset displayed keys to handle new values
        $this->displayed_keys = array();
    }

    /**
     * hidden params processing function
     * called by process_params()
     */
    public function process_hidden_params()
    {
        foreach ($this->hidden_fields as $key => $value) {
            if ($this->hidden_fields[$key] === null) {
                $this->hidden_fields[$key] = new ffData($this->parent[0]->retrieve_param($this->id, $key));
            }
            $this->hidden_params = $this->prefix . $key . "=" . $this->hidden_fields[$key]->getValue(null, FF_SYSTEM_LOCALE) . "&";
        }
        reset($this->hidden_fields);
    }

    /**
     * navigator params processing function
     * called by process_params()
     */
    public function process_navigator_params()
    {
        if (!$this->use_paging) {
            return;
        }
        
        $this->page = intval($this->parent[0]->retrieve_param($this->id, $this->navigator[0]->page_parname));
        $this->records_per_page = intval($this->parent[0]->retrieve_param($this->id, $this->navigator[0]->records_per_page_parname));

        if (!$this->records_per_page) {
            $this->records_per_page = $this->default_records_per_page;
        }
        if (!$this->page) {
            $this->page = $this->default_page;
        }
        if (!$this->nav_selector_elements) {
            $this->nav_selector_elements = $this->default_nav_selector_elements;
        }

        $this->tpl[0]->set_var("page", $this->page);
        $this->tpl[0]->set_var("records_per_page", $this->records_per_page);

        $this->navigator_params = $this->prefix . "records_per_page=" . $this->records_per_page . "&" . $this->prefix . "page=" . $this->page;
    }

    /**
     * alpha params processing function
     * called by process_params()
     */
    public function process_alpha_params()
    {
        if (!$this->use_alpha) {
            return;
        }

        if ($this->parent[0]->isset_param($this->id, "alpha")) {
            $this->alpha = $this->parent[0]->retrieve_param($this->id, "alpha");
        } else {
            $this->alpha = $this->alpha_default;
        }

        $this->tpl[0]->set_var("actual_alpha", $this->alpha);
        if (strlen($this->search_params)) {
            $this->search_params .= "&";
        }
        $this->search_params .= $this->prefix . "alpha=" . urlencode($this->alpha);

        if (strlen($this->alpha)) {
            $tmp_alpha_cont = null;
            if (isset($this->search_fields[$this->alpha_field])) {
                $tmp_alpha_cont = $this->search_fields;
            } elseif (isset($this->grid_fields[$this->alpha_field])) {
                $tmp_alpha_cont = $this->grid_fields;
            }
            
            if ($tmp_alpha_cont === null) {
                ffErrorHandler::raise("Cannot find alpha field into fields", E_USER_ERROR, $this, get_defined_vars());
            }

            $tmp_field = "";
            
            if (strlen($tmp_alpha_cont[$this->alpha_field]->src_table)) {
                $tmp_field .= "`" . $tmp_alpha_cont[$this->alpha_field]->src_table . "`.";
            }

            $tmp_field .=  "`" . $tmp_alpha_cont[$this->alpha_field]->get_data_source(false) . "`";

            switch ($this->alpha) {
                case "cipher":
                    //$tmp_sql .= " SUBSTRING( " . $tmp_field . ", 1, 1 ) BETWEEN '0' AND '9' ";
                    $tmp_sql .= $tmp_field . " REGEXP '^[0-9]' ";
                    break;
                default:
                    $tmp_sql .= $tmp_field . " LIKE '" . $this->db[0]->toSql($this->alpha, "Text", false) . "%' ";
            }

            if ($this->search_fields[$this->alpha_field]->src_having) {
                if (strlen($this->sqlHaving)) {
                    $this->sqlHaving .= " AND ";
                }

                $this->sqlHaving .= $tmp_sql;
            } else {
                if (strlen($this->sqlWhere)) {
                    $this->sqlWhere .= " AND ";
                }

                $this->sqlWhere .= $tmp_sql;
            }
        }
    }

    /**
     * search params processing function
     * called by process_params()
     */
    public function process_search_params()
    {
        if (!$this->use_search) {
            return;
        }

        if (strlen($this->parent[0]->retrieve_param($this->id, "searched"))) {
            $this->searched = true;
        }

        foreach ($this->search_fields as $key => $FormField) {
            if ($this->search_fields[$key]->skip_search) {
                continue;
            }
            
            if (strlen($this->search_fields[$key]->src_table)) {
                $tblprefix = "`" . $this->search_fields[$key]->src_table . "`.";
            } else {
                $tblprefix = "";
            }

            if ($this->search_fields[$key]->src_interval) {
                // parse from field
                $this->search_fields[$key]->interval_from_value->setValue(
                    $this->parent[0]->retrieve_param($this->id, $this->search_fields[$key]->id . "_from_src"),
                    $this->search_fields[$key]->get_app_type(),
                    $this->search_fields[$key]->get_locale()
                );

                if (strlen($this->search_fields[$key]->interval_from_value->ori_value)) {
                    $this->searched = true;
                }

                // parse to field
                $this->search_fields[$key]->interval_to_value->setValue(
                    $this->parent[0]->retrieve_param($this->id, $this->search_fields[$key]->id . "_to_src"),
                    $this->search_fields[$key]->get_app_type(),
                    $this->search_fields[$key]->get_locale()
                                                                        );

                if (strlen($this->search_fields[$key]->interval_to_value->ori_value)) {
                    $this->searched = true;
                }
            } else {
                if ($this->parent[0]->isset_param($this->id, $this->search_fields[$key]->id . "_src")) {
                    $this->search_fields[$key]->setValue(
                        $this->parent[0]->retrieve_param($this->id, $this->search_fields[$key]->id . "_src"),
                        $this->search_fields[$key]->get_app_type(),
                        $this->search_fields[$key]->get_locale()
                                                        );
                } else {
                    switch ($this->search_fields[$key]->extended_type) {
                        case "Boolean":
                            $this->search_fields[$key]->value = $this->search_fields[$key]->unchecked_value;
                            break;

                        default:
                            $this->search_fields[$key]->value = $this->search_fields[$key]->getDefault(array(&$this));
                    }
                }

                if (strlen($this->search_fields[$key]->value->ori_value)) {
                    $this->searched = true;
                }
            }

            $tmp_sql = "";
            if ($this->search_fields[$key]->src_interval && $this->search_fields[$key]->data_type != "") {
                // check validity
                $bFindNameTag = preg_match("/\[NAME\]/", $this->search_fields[$key]->src_operation);
                $bFindValueTag = preg_match("/\[VALUE\]/", $this->search_fields[$key]->src_operation);

                if ($bFindValueTag) {
                    die("You must enter a valid src_operation to use interval search");
                }

                if (strlen($this->search_fields[$key]->interval_from_value->ori_value) || strlen($this->search_fields[$key]->interval_to_value->ori_value)) {
                    if (strlen($this->search_params)) {
                        $this->search_params .= "&";
                    }

                    $tmp_sql = " (";

                    if (strlen($this->search_fields[$key]->interval_from_value->ori_value)) {
                        $this->search_params .= $this->prefix . $this->search_fields[$key]->id . "_from_src=" . $this->search_fields[$key]->interval_from_value->ori_value;
                        $tmp_where_value = "";
                        if ($this->search_fields[$key]->src_prefix || $this->search_fields[$key]->src_postfix) {
                            $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->interval_from_value, $this->search_fields[$key]->base_type, false);
                            $tmp_where_value = "'" . $this->search_fields[$key]->src_prefix . $tmp_where_value . $this->search_fields[$key]->src_postfix . "'";
                        } else {
                            $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->interval_from_value, $this->search_fields[$key]->base_type);
                        }
                        $tmp_sql .= " " . str_replace("[NAME]", $tblprefix . "`" . $this->search_fields[$key]->get_data_source() . "`", $this->search_fields[$key]->src_operation) . " >= " . $tmp_where_value;
                    }

                    if (strlen($this->search_fields[$key]->interval_to_value->ori_value)) {
                        if (strlen($this->search_fields[$key]->interval_from_value->ori_value)) {
                            $tmp_sql .= " AND ";
                            $this->search_params .= "&";
                        }
                        $this->search_params .= $this->prefix . $this->search_fields[$key]->id . "_to_src=" . $this->search_fields[$key]->interval_to_value->ori_value;
                        $tmp_where_value = "";
                        if ($this->search_fields[$key]->src_prefix || $this->search_fields[$key]->src_postfix) {
                            $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->interval_to_value, $this->search_fields[$key]->base_type, false);
                            $tmp_where_value = "'" . $this->search_fields[$key]->src_prefix . $tmp_where_value . $this->search_fields[$key]->src_postfix . "'";
                        } else {
                            $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->interval_to_value, $this->search_fields[$key]->base_type);
                        }
                        $tmp_sql .= " " . str_replace("[NAME]", $tblprefix . "`" . $this->search_fields[$key]->get_data_source() . "`", $this->search_fields[$key]->src_operation) . " <= " . $tmp_where_value;
                    }

                    $tmp_sql .= ") ";
                }
            } elseif ($this->search_fields[$key]->data_type != "") {
                if (strlen($this->search_fields[$key]->value->ori_value)) {
                    if (strlen($this->search_params)) {
                        $this->search_params .= "&";
                    }

                    $this->search_params .= $this->prefix . $this->search_fields[$key]->id . "_src=" . $this->search_fields[$key]->getValue();

                    $tmp_where_value = "";

                    if ($this->search_fields[$key]->src_prefix || $this->search_fields[$key]->src_postfix) {
                        $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->value, $this->search_fields[$key]->base_type, false);
                        $tmp_where_value = "'" . $this->search_fields[$key]->src_prefix . $tmp_where_value . $this->search_fields[$key]->src_postfix . "'";
                    } else {
                        $tmp_where_value = $this->db[0]->toSql($this->search_fields[$key]->value);
                    }

                    if (is_array($this->search_fields[$key]->src_fields) && count($this->search_fields[$key]->src_fields)) {
                        $tmp_sql .= " ( ";
                    }

                    $tmp_where_part = $this->search_fields[$key]->src_operation;
                    $tmp_where_part = str_replace("[NAME]", $tblprefix . "`" . $this->search_fields[$key]->get_data_source() . "`", $tmp_where_part);
                    $tmp_where_part = str_replace("[VALUE]", $tmp_where_value, $tmp_where_part);
                    $tmp_sql .= " " . $tmp_where_part . " ";

                    if (is_array($this->search_fields[$key]->src_fields) && count($this->search_fields[$key]->src_fields)) {
                        foreach ($this->search_fields[$key]->src_fields as $addfld_key => $addfld_value) {
                            $tmp_where_part = $this->search_fields[$key]->src_operation;
                            $tmp_where_part = str_replace("[NAME]", $addfld_value, $tmp_where_part);
                            $tmp_where_part = str_replace("[VALUE]", $tmp_where_value, $tmp_where_part);
                            $tmp_sql .= " OR " . $tmp_where_part . " ";
                        }
                        reset($this->search_fields[$key]->src_fields);
                        $tmp_sql .= " ) ";
                    }
                }
            } else {
                if (strlen($this->search_params)) {
                    $this->search_params .= "&";
                }
                $this->search_params .= $this->prefix . $this->search_fields[$key]->id . "_src=" . $this->search_fields[$key]->getValue();
            }

            if (strlen($tmp_sql)) {
                if ($this->search_fields[$key]->src_having) {
                    if (strlen($this->sqlHaving)) {
                        $this->sqlHaving .= " AND ";
                    }

                    $this->sqlHaving .= $tmp_sql;
                } else {
                    if (strlen($this->sqlWhere)) {
                        $this->sqlWhere .= " AND ";
                    }

                    $this->sqlWhere .= $tmp_sql;
                }
            }
        }
        reset($this->search_fields);
        // after retrieving values, build sql where based on user function, is specified

        $res = $this->doEvent("extended_search", array(&$this, $this->sqlWhere, $this->sqlHaving));
        $rc = end($res);
        if ($rc !== null) {
            if (is_array($rc)) {
                $this->sqlWhere = $rc[0];
                $this->sqlHaving = $rc[1];
            } else {
                $this->sqlWhere = $rc;
            }
        }
    }

    /**
     * order params processing function
     * called by process_params()
     */
    public function process_order_params()
    {
        if (!strlen($this->order_default)) {
            die("You MUST set an order by default!");
        }

        $this->order = $this->parent[0]->retrieve_param($this->id, "order");
        $this->direction = $this->parent[0]->retrieve_param($this->id, "direction");

        if (!strlen($this->order)) {
            $is_default = true;
            $this->order = $this->order_default;
        }
        
        $tmp = null;
        if (isset($this->key_fields[$this->order])) {
            $tmp = $this->key_fields[$this->order];
        } elseif (isset($this->grid_fields[$this->order])) {
            $tmp = $this->grid_fields[$this->order];
        }

        if ($tmp === null) {
            ffErrorHandler::raise("Cannot determine order field!", E_USER_ERROR, $this, get_defined_vars());
        }

        if (!strlen($this->direction)) {
            $this->direction = $tmp->order_dir;
        }

        // build order SQL
        if (strlen($tmp->order_SQL)) {
            if ($is_default) {
                $this->sqlOrder = " " . str_replace("[ORDER_DIR]", $this->direction, $tmp->order_SQL); //. " " .  $this->direction . " ";
            } else {
                if ($this->order != $this->order_default) {
                    $this->sqlOrder = " " . $tmp->get_order_field() . " " .  $this->direction . " ";
                } else {
                    $this->sqlOrder = " " . str_replace("[ORDER_DIR]", $this->direction, $tmp->order_SQL); //. " " .  $this->direction . " ";
                }
            }
        } else {
            $this->sqlOrder = " " . $tmp->get_order_field() . " " .  $this->direction . " ";
        }
        if ($this->tpl !== null) {
            $this->tpl[0]->set_var("actual_order", $this->order);
            $this->tpl[0]->set_var("actual_direction", $this->direction);

            $this->order_params = $this->prefix . "order=" . $this->order . "&" . $this->prefix . "direction=" . $this->direction;

            if ($this->use_order) {
                $this->tpl[0]->parse("SectHiddenOrder", false);
            } else {
                $this->tpl[0]->set_var("SectHiddenOrder", "");
            }
        }
    }

    public function parse_hidden_field()
    {
        $this->tpl[0]->parse("SectHiddenField", true);
        $this->parsed_hidden_fields++;
    }
    /**
     * hidden params template processing function
     * called by process_interface()
     */
    public function process_hidden()
    {
        foreach ($this->hidden_fields as $key => $value) {
            $this->tpl[0]->set_var("id", $key);
            $this->tpl[0]->set_var("value", ffCommon_specialchars($value->getValue(null, FF_SYSTEM_LOCALE)));
            $this->parse_hidden_field();
        }
        reset($this->hidden_fields);
    }

    /**
     * grid template preparation processing function
     * called by process_interface()
     */
    public function process_grid()
    {
        if ($this->display_grid == "never" || ($this->display_grid == "search" && !strlen($this->searched))) {
            $this->tpl[0]->set_var("SectGrid", "");
            return;
        }

        $res = $this->doEvent("on_before_process_grid", array(&$this, $this->tpl[0]));

        //$this->process_labels(); 	// also do order, because order is embedded with labels

        if ($this->tpl !== null) {
            if ($this->display_new) { // done at this time due to maxspan
                if (strlen($this->bt_insert_url)) {
                    $temp_url = ffProcessTags($this->bt_insert_url, $this->key_fields, $this->grid_fields, "normal", $this->parent[0]->get_params(), rawurlencode($_SERVER['REQUEST_URI']), $this->parent[0]->get_globals());
                } else {
                    if (strlen($this->record_insert_url)) {
                        $temp_url = $this->record_insert_url;
                    } else {
                        $temp_url = $this->record_url;
                    }
                    $temp_url .= "?" . $this->parent[0]->get_keys($this->key_fields) .
                                $this->parent[0]->get_globals() . $this->addit_insert_record_param .
                                "ret_url=" . rawurlencode($_SERVER['REQUEST_URI']);
                }
                $this->tpl[0]->set_var("addnew_url", ffCommon_specialchars($temp_url));
                $this->tpl[0]->parse("SectAddNew", false);
                $this->tpl[0]->set_var("SectNotAddNew", "");
            } else {
                $this->tpl[0]->set_var("SectAddNew", "");
                if ($this->display_not_add_new) {
                    $this->tpl[0]->parse("SectNotAddNew", false);
                } else {
                    $this->tpl[0]->set_var("SectNotAddNew", "");
                }
            }
        }

        if (strlen($this->processed_SQL)) {
            $this->db[0]->query($this->processed_SQL);
        }
        $res = $this->doEvent("on_process_grid", array(&$this, $this->tpl[0]));
    }

    /**
     * action buttons processing function
     * called by process_interface()
     */
    public function process_action_buttons()
    {
        if (!$this->display_actions) {
            $this->tpl[0]->set_var("SectActionButtons", "");
            return;
        }

        $count = 0;
        if (is_array($this->action_buttons) && count($this->action_buttons)) {
            foreach ($this->action_buttons as $key => $FormButton) {
                if (!isset($this->buttons_options[$key]["display"]) || $this->buttons_options[$key]["display"] !== false) {
                    $this->tpl[0]->set_var("ActionButton", $this->action_buttons[$key]->process());
                    $this->tpl[0]->parse("SectAction", true);
                    $count++;
                }
            }
            reset($this->action_buttons);
            $this->tpl[0]->parse("SectActionButtons", false);
        }
        
        if ($count) {
            $this->tpl[0]->parse("SectActionButtons", false);
        } else {
            $this->tpl[0]->set_var("SectAction", "");
            $this->tpl[0]->set_var("SectActionButtons", "");
        }
    }
    /**
     * action buttons processing function
     * called by process_interface()
     */
    public function process_action_buttons_header()
    {
        if (!$this->display_actions) {
            $this->tpl[0]->set_var("SectActionButtonsHeader", "");
            return;
        }

        $count = 0;
        if (is_array($this->action_buttons_header) && count($this->action_buttons_header)) {
            foreach ($this->action_buttons_header as $key => $FormButton) {
                if (!isset($this->buttons_options[$key]["display"]) || $this->buttons_options[$key]["display"] !== false) {
                    $this->tpl[0]->set_var("ActionButtonHeader", $this->action_buttons_header[$key]->process());
                    $this->tpl[0]->parse("SectActionHeader", true);
                    $count++;
                }
            }
            reset($this->action_buttons_header);
        }
        
        if ($count) {
            $this->tpl[0]->parse("SectActionButtonsHeader", false);
        } else {
            $this->tpl[0]->set_var("SectActionHeader", "");
            $this->tpl[0]->set_var("SectActionButtonsHeader", "");
        }
    }

    /**
     * action processing function
     * called by process()
     */
    public function process_action()
    {
        if (strlen($this->frmAction)) {
            $res = $this->doEvent("on_do_action", array(&$this, $this->frmAction));
            if ($rc = end($res)) {
                return;
            }
        }
    }

    /**
     * template set properties
     * called by process_interface()
     */
    public function setProperties()
    {
        if (is_array($this->properties) && count($this->properties)) {
            foreach ($this->properties as $key => $value) {
                if (is_array($this->properties[$key]) && count($this->properties[$key])) {
                    do {
                        $subkey = key($this->properties[$key]);
                        if ($this->properties[$key][$subkey] !== null) {
                            $this->tpl[0]->set_var($key . "_" . $subkey, $this->properties[$key][$subkey]);
                            $this->tpl[0]->parse("Sect" . $key . "_" . $subkey, false);
                        } else {
                            $this->tpl[0]->set_var("Sect" . $key . "_" . $subkey, "");
                        }
                    } while (next($this->properties[$key]) !== false);
                    reset($this->properties[$key]);
                }
            }
            reset($this->properties);
        }
    }

    /**
     * Esegue il redirect all'url specificato
     * @param String $url
     */
    public function redirect($url, $response = array())
    {
        ffRedirect($url, null, null, $response);
    }

    /**
     *
     * @param boolean $returnurl se dev'essere restituito l'url o effettuato il redirect immediato
     * @param String $type il tipo di dialog (okonly, yesno..)
     * @param String $title il titolo del dialog
     * @param String $message il messaggio visualizzato
     * @param String $cancelurl l'url di negazione/ritorno
     * @param String $confirmurl l'url di conferma/proseguimento
     * @return String
     */
    public function dialog($returnurl = false, $type, $title, $message, $cancelurl, $confirmurl)
    {
        if ($this->dialog_path === null) {
            $dialog_path = $this->parent[0]->getThemePath() . "/ff/dialog";
        } else {
            $dialog_path = $this->parent[0]->site_path . $this->dialog_path;
        }
            
        $message = ffProcessTags($message, $this->key_fields, $this->grid_fields, "normal");

        $res = $this->doEvent("onDialog", array($this, $returnurl, $type, $title, $message, $cancelurl, $confirmurl, $dialog_path));
        $ret = end($res);
        if ($ret === null) {
            $ret = ffDialog($returnurl, $type, $title, $message, $cancelurl, $confirmurl, $dialog_path);
        }

        if ($returnurl) {
            return $ret;
        } else {
            $this->redirect($ret);
        }
    }

    /**
     * inizializza i pulsanti di default
     */
    public function initControls()
    {
        // PREPARE DEFAULT BUTTONS
        if ($this->buttons_options["search"]["display"]) {
            if ($this->buttons_options["search"]["obj"] !== null) {
                $this->addSearchButton($this->buttons_options["search"]["obj"], $this->buttons_options["search"]["index"]);
            } else {
                $tmp = ffButton::factory(null, $this->disk_path, $this->site_path, $this->page_path, $this->theme);
                $tmp->id 			= "searched";
                $tmp->label 		= $this->buttons_options["search"]["label"];
                $tmp->aspect 		= "link";
                $tmp->action_type 	= "submit";
                $tmp->frmAction		= "search";
                $this->addSearchButton($tmp, $this->buttons_options["search"]["index"]);
            }
        }
        if ($this->buttons_options["export"]["display"]) {
            if (!strlen($this->buttons_options["export"]["label"])) {
                $this->buttons_options["export"]["label"] = ffTemplate::_get_word_by_code("ffGrid_export");
            }

            if ($this->buttons_options["export"]["obj"] !== null) {
                $this->addActionButtonHeader($this->buttons_options["export"]["obj"]);
            } else {
                $tmp = ffButton::factory(null, $this->disk_path, $this->site_path, $this->page_path, $this->theme);
                $tmp->id 			= "export";
                $tmp->label 		= $this->buttons_options["export"]["label"];
                $tmp->aspect 		= "link";
                $tmp->action_type 	= "submit";
                $tmp->frmAction		= "export";
                //$tmp->aspect 		= "link";
                //$tmp->action_type 	= "gotourl";
                if (strlen($tmp->class)) {
                    $tmp->class .= " ";
                }
                $tmp->class .= "noactivebuttons";
                //$tmp->form_action_url = $this->parent[0]->getRequestUri() . "&" . $this->id . "_t=xls";
                //$tmp->jsaction = "ff.ajax.doRequest({'component' : '" . $this->id . "', 'addFields' : '" . $this->id . "t=xls'});";
                //$tmp->class 		.= "noactivebuttons";
                //$tmp->url = $this->parent[0]->getRequestUri() . "&" . $this->id . "_t=xls";
                $this->addActionButtonHeader($tmp);
            }
        }
    }
    
    public function setWidthComponent($resolution_large_to_small)
    {
        if (is_array($resolution_large_to_small) || is_numeric($resolution_large_to_small)) {
            $this->framework_css["component"]["grid"] = ffCommon_setClassByFrameworkCss($resolution_large_to_small);
        } elseif (strlen($resolution_large_to_small)) {
            $this->framework_css["component"]["grid"] = $resolution_large_to_small;
        } else {
            $this->framework_css["component"]["grid"] = false;
        }
    }
}
