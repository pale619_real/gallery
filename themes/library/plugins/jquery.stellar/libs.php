<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"stellar" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.stellar",
							"file" => "jquery.stellar.js"
						)
					)
				)
			)
		)
	)
);
