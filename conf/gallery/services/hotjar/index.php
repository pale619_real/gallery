<?php
if($actual_srv["enable"])
{
    $parse_script = false;
    $arrPath = explode("/", trim($cm->path_info, "/"));
    switch($arrPath[0]) {
        case "index":
            $parse_script = true;
            break;
        case "":
        case "search":
        case "registrazione":
        case "activation":
        case "recover":
            $parse_script = false;
            break;
        case "user":
            switch($arrPath[1]) {
                case "teleconsulto":
                    switch($arrPath[2]) {
                        case "dettaglio":
                            $parse_script = false;
                            break;
                    }
                    break;
                default:
                    break;
            }
            break;
        case "medici-online":
            switch($arrPath[1]) {
                case "":
                case "search":
                    $parse_script = false;
                    break;
                default:
                    break;
            }
            break;
        case "esperto-risponde":
            switch($arrPath[1]) {
                case "search":
                case "archivio":
                    $parse_script = false;
                    break;
                case "":
                    $parse_script = false;
                    break;
                default:
                    break;
            }
            break;
        case "pro":
            switch($arrPath[1]) {
                case "medsurf":
                    break;
                default:
                    $parse_script = false;
                    break;
            }
            break;
        case "news-ed-eventi":
            break;
    }
    if($parse_script) {
        $js_content = "(function(h,o,t,j,a,r){
                        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                        h._hjSettings={hjid:506120,hjsv:6};
                        a=o.getElementsByTagName('head')[0];
                        r=o.createElement('script');r.async=1;
                        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                        a.appendChild(r);
                    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');";
        $oPage->tplAddJs("hotjar", null, null, false, $oPage->isXHR(), $js_content, true, "bottom");
    }

}