<?php

class ffWidget_dragsort extends ffCommon
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $template_file 	 = "ffWidget.html";

    public $class			= "ffWidget_dragsort";

    public $widget_deps	= array();
    public $js_deps = array(
                              "jquery" 			=> null
                        );
    public $css_deps = array();
    // PRIVATE VARS

    public $source_path		= null;
    public $oPage				= null;
    public $style_path			= null;

    public $tpl 				= null;

    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        $this->get_defaults();

        $this->oPage = array(&$oPage);

        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->style_path = $style_path;
    }

    public function prepare_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("site_path", $this->oPage[0]->site_path);
        $this->tpl[$id]->set_var("theme", $this->oPage[0]->getTheme());

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }

        if (strlen($_SERVER["QUERY_STRING"])) {
            $this->tpl[$id]->set_var("query_string", "?" . $_SERVER["QUERY_STRING"]);
        }
    }

    public function process(ffDetails_base $details, $options, $key_field)
    {
        $tpl_id = $details->id;
        if (!isset($this->tpl[$tpl_id])) {
            $this->prepare_template($tpl_id);
        }
            
        $details->addEvent("on_before_parse_row", "ffWidget_dragsort::on_before_parse_row", ffEvent::PRIORITY_HIGH, 0, null, null, array(&$this, $options["resource_id"], $key_field));

        $this->tpl[$tpl_id]->set_var("component_id", $details->id);
        $this->tpl[$tpl_id]->set_var("resource_id", $options["resource_id"]);
        $this->tpl[$tpl_id]->set_var("service_path", $options["service_path"]);
        
        $this->tpl[$tpl_id]->parse("SectIstance", true);
    }

    public function get_component_headers($id)
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddCss("ff.ffDetails.dragsort", "ff.ffDetails.dragsort.css", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
            $this->oPage[0]->tplAddJs("jquery.fn.jTableOrder", "jTableOrder.js", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
            $this->oPage[0]->tplAddJs("ff.ffDetails", "ffDetails.js", FF_THEME_DIR . "/library/ff");
            $this->oPage[0]->tplAddJs("ff.ffDetails.dragsort", "dragsort.js", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
        }

        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectHeaders", false);
    }

    public function get_component_footers($id)
    {
        if (!isset($this->tpl[$id])) {
            return;
        }

        return $this->tpl[$id]->rpparse("SectFooters", false);
    }
    
    public function process_headers()
    {
        if ($this->oPage !== null) { //code for ff.js
            $this->oPage[0]->tplAddCss("ff.ffDetails.dragsort", "ff.ffDetails.dragsort.css", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
            $this->oPage[0]->tplAddJs("jquery.fn.jTableOrder", "jTableOrder.js", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
            $this->oPage[0]->tplAddJs("ff.ffDetails", "ffDetails.js", FF_THEME_DIR . "/library/ff");
            $this->oPage[0]->tplAddJs("ff.ffDetails.dragsort", "dragsort.js", FF_THEME_DIR . "/responsive/ff/ffDetails/widgets/dragsort");
        }
                
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectHeaders", false);
    }
    
    public function process_footers()
    {
        if (!isset($this->tpl["main"])) {
            return;
        }

        return $this->tpl["main"]->rpparse("SectFooters", false);
    }

    public function prepare_details_template($id)
    {
        $this->tpl[$id] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[$id]->load_file($this->template_file, "main");

        $this->tpl[$id]->set_var("site_path", $this->oPage[0]->site_path);
        $this->tpl[$id]->set_var("theme", $this->oPage[0]->getTheme());

        $this->tpl[$id]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[$id]->set_var("style_path", $this->style_path);
        } elseif ($this->oPage !== null) {
            $this->tpl[$id]->set_var("style_path", $this->oPage[0]->getThemePath());
        }

        if (strlen($_SERVER["QUERY_STRING"])) {
            $this->tpl[$id]->set_var("query_string", "?" . $_SERVER["QUERY_STRING"]);
        }
    }
    public static function on_before_parse_row(ffDetails_horiz $details, $row, ffWidget_dragsort $plugin, $resource_id, $key_field)
    {
        $tpl_id = $details->id;
        if (!isset($plugin->tpl[$tpl_id])) {
            $plugin->prepare_details_template($tpl_id);
        }

        $plugin->tpl[$tpl_id]->set_var("component_id", $details->id);
        $plugin->tpl[$tpl_id]->set_var("key_value", $details->key_fields[$key_field]->getValue());
        $plugin->tpl[$tpl_id]->parse("SectData", true);
        return null;
    }
}
