jQuery(document).ready(function() {
	jQuery("img").addClass("corner iradius16");
});

/*
* Initialisation class "corner"
* vary the radius by adding iradius followed by the desired radius in pixel:
  Corner radius class "iradius24" - min=0 max=100 default=0
* vary the shading by adding ishade followed by the desired opacity in percentage:
  Image shading class "ishade50" - min=0 max=100 default=0
* vary the shadow by adding ishadow followed by the desired opacity in percentage:
  Image shadow class "ishadow33" - min=0 max=100 default=0
* vary the shadow by adding inverse:
  Shadow invert class "inverse"
*/