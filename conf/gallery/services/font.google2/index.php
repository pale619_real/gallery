<?php
    // $db_gallery : access db object
    // $globals : globals settings
    // $actual_srv = params defined by system
    if ($actual_srv["enable"]) {
        $string_font = "";
        foreach ($actual_srv as $key => $value) {
            if (strpos($key, "font") === 0 && $value) {
                if(strlen($string_font)) {
                    $string_font .= "&";
                }
                $string_font .= "family=" . $value;
            }
        }



        if(strlen($string_font)) {
            $oPage->tplAddCss("google-font-v2", "css2?" . $string_font . "&display=swap", "https://fonts.googleapis.com");
        }
    }
